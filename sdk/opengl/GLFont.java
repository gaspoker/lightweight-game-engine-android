package sdk.opengl;

public class GLFont {
    public final GLTexture texture;
    public final int glyphWidth;
    public final int glyphHeight;
    public final GLTextureRegion[] glyphs = new GLTextureRegion[96];

    public GLFont(GLTexture texture, int offsetX, int offsetY, int glyphsPerRow, int glyphWidth, int glyphHeight) {
        this.texture = texture;
        this.glyphWidth = glyphWidth;
        this.glyphHeight = glyphHeight;
        int x = offsetX;
        int y = offsetY;
        for(int i = 0; i < 96; i++) {
            glyphs[i] = new GLTextureRegion(texture, x, y, glyphWidth, glyphHeight);
            x += glyphWidth;
            if(x == offsetX + glyphsPerRow * glyphWidth) {
                x = offsetX;
                y += glyphHeight;
            }
        }        
    }

    public void drawText(GLSpriteBatcher batcher, String text, float x, float y, int color, float alpha) {
        int len = text.length();
        for(int i = 0; i < len; i++) {
            int c = text.charAt(i) - ' ';
            if(c < 0 || c > glyphs.length - 1) 
                continue;
            
            GLTextureRegion glyph = glyphs[c];
            batcher.drawSprite(x, y, glyphWidth, glyphHeight, glyph, color, alpha);
            x += glyphWidth;
        }
    }
}
