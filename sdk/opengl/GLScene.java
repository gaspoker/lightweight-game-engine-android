package sdk.opengl;

import sdk.base.IGame;
import sdk.core.Scene;

public abstract class GLScene extends Scene {
    protected final GLGraphics glGraphics;
    protected final GLGame glGame;
    
    public GLScene(IGame game) {
        super(game);
        glGame = (GLGame)game;
        glGraphics = ((GLGame)game).getGLGraphics();
    }
}
