package sdk.base;

import sdk.core.Scene;

public interface IGame {
    public IInput getInput();

    public IFileIO getFileIO();

    public IGraphics getGraphics();

    public IAudio getAudio();

    public void setScene(Scene scene);

    public Scene getScene();

    public Scene getMainScene();

    public void loadAssets();
}