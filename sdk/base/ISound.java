package sdk.base;

public interface ISound {
    public void play(float volume, boolean loop);
    public void pause();
    public void stop();
    //public void pauseAll();
    public void dispose();
}
