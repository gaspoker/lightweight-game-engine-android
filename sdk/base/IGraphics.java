package sdk.base;

import sdk.base.IPixmap;

public interface IGraphics {
    public static enum PixmapFormat {
        ARGB8888, ARGB4444, RGB565
    }

    public IPixmap newPixmap(String fileName, PixmapFormat format);

    public void clear(int color);

    public void drawPixel(float x, float y, int color, float alpha);

    public void drawLine(float x, float y, float x2, float y2, int color, float alpha);

    public void drawRect(float x, float y, float width, float height, int color, float alpha);

    public void drawPixmap(IPixmap pixmap, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight);

    public void drawPixmap(IPixmap pixmap, int x, int y);

    public int getWidth();

    public int getHeight();
}
