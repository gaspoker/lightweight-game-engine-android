package sdk.base;

import java.util.List;

import android.view.View.OnTouchListener;

import sdk.base.IInput.TouchEvent;

public interface ITouchHandler extends OnTouchListener {
    public boolean isTouchDown(int pointer);
    
    public int getTouchX(int pointer);
    
    public int getTouchY(int pointer);
    
    public List<TouchEvent> getTouchEvents();

    //public void setScale(float scaleX, float scaleY);
}
