package sdk.base;

import android.graphics.Bitmap;

import sdk.base.IGraphics.PixmapFormat;

public class Pixmap implements IPixmap {
    public Bitmap bitmap;
    PixmapFormat format;
    
    public Pixmap(Bitmap bitmap, PixmapFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }

    public int getWidth() {
        return bitmap.getWidth();
    }

    public int getHeight() {
        return bitmap.getHeight();
    }

    public PixmapFormat getFormat() {
        return format;
    }

    public void dispose() {
        bitmap.recycle();
    }      
}
