package sdk.base;

import sdk.base.IGraphics.PixmapFormat;

public interface IPixmap {
    public int getWidth();

    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
