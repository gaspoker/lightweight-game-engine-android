package sdk.base;

public interface IAudio {
    public IMusic newMusic(String filename);

    public ISound newSound(String filename);
    //public ISound newSound(String filename, long duration);

    public void stopAll();

    public void pause();

    public void resume();

    public boolean isPlayingMusic();
}
