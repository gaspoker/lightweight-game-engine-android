package sdk.base;

public interface IMusic {
    public void play();

    public void play(float volume, boolean isLooping);

    public void stop();

    public void pause();

    public void setLooping(boolean looping);

    public void setVolume(float volume);

    public boolean isPlaying();

    public boolean isStopped();

    public boolean isLooping();

    public void dispose();
}
