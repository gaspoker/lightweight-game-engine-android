package sdk.base;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import sdk.base.IInput.TouchEvent;
import sdk.base.Pool.PoolObjectFactory;

@TargetApi(5)
public class MultiTouchHandler implements ITouchHandler {
    private static final int MAX_TOUCHPOINTS = 3; // Antes 10
	
    boolean[] isTouched = new boolean[MAX_TOUCHPOINTS];
    int[] touchX = new int[MAX_TOUCHPOINTS];
    int[] touchY = new int[MAX_TOUCHPOINTS];
    int[] id = new int[MAX_TOUCHPOINTS];
    Pool<TouchEvent> touchEventPool;
    List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();
    List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();
    float scaleX;
    float scaleY;
    final float slop;
    int initialX = 0;
    int initialY = 0;
    long startDragging;

    public MultiTouchHandler(View view, float scaleX, float scaleY) {
        PoolObjectFactory<TouchEvent> factory = new PoolObjectFactory<TouchEvent>() {
            public TouchEvent createObject() {
                return new TouchEvent();
            }
        };
        this.touchEventPool = new Pool<TouchEvent>(factory, 100);

        this.slop = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.startDragging = 0;

        view.setOnTouchListener(this);

        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    public boolean onTouch(View v, MotionEvent event) {
        synchronized (this) {
            //gestureDetector.onTouchEvent(event);

            int action = event.getAction() & MotionEvent.ACTION_MASK;
            int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
            int pointerCount = event.getPointerCount();
            //Log.d("PAD-TEST", "POINTER COUNT: " + pointerCount + " -----------------------------------------------------------------");
            TouchEvent touchEvent;
            for (int i = 0; i < MAX_TOUCHPOINTS; i++) {
                if (i >= pointerCount) {
                    isTouched[i] = false;
                    id[i] = -1;
                    continue;
                }
                int pointerId = event.getPointerId(i);
                if (event.getAction() != MotionEvent.ACTION_MOVE && i != pointerIndex) {
                    // if it's an up/down/cancel/out event, mask the id to see if we should process it for this touch
                    // point
                    continue;
                }
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_POINTER_DOWN:
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = TouchEvent.TOUCH_DOWN;
                        touchEvent.dragDirection = TouchEvent.DRAG_NONE;
                        touchEvent.dragDuration = 0;
                        touchEvent.pointer = pointerId;
                        touchEvent.x = touchX[i] = (int) (event.getX(i) * scaleX);
                        touchEvent.y = touchY[i] = (int) (event.getY(i) * scaleY);
                        isTouched[i] = true;
                        id[i] = pointerId;
                        touchEventsBuffer.add(touchEvent);

                        initialX = touchX[i]; //(int) (event.getX(i) * scaleX);
                        initialY = touchY[i]; //(int) event.getY(i);
                        startDragging = System.nanoTime();
                        break;

                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                    case MotionEvent.ACTION_CANCEL:
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = TouchEvent.TOUCH_UP;
                        touchEvent.dragDirection = TouchEvent.DRAG_NONE;
                        touchEvent.dragDuration = (System.nanoTime() - startDragging) / 1000000; // in millis
                        touchEvent.pointer = pointerId;
                        touchEvent.x = touchX[i] = (int) (event.getX(i) * scaleX);
                        touchEvent.y = touchY[i] = (int) (event.getY(i) * scaleY);
                        isTouched[i] = false;
                        id[i] = -1;
                        touchEventsBuffer.add(touchEvent);
                        //Log.d("PAD-TEST", "ACTION_UP");
                        break;

                    // https://developer.android.com/reference/android/view/MotionEvent.html
                    case MotionEvent.ACTION_HOVER_EXIT:
                    case MotionEvent.ACTION_OUTSIDE:
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = TouchEvent.TOUCH_OUT;
                        touchEvent.dragDirection = TouchEvent.DRAG_NONE;
                        touchEvent.dragDuration = (System.nanoTime() - startDragging) / 1000000; // in millis
                        touchEvent.pointer = pointerId;
                        touchEvent.x = touchX[i] = (int) (event.getX(i) * scaleX);
                        touchEvent.y = touchY[i] = (int) (event.getY(i) * scaleY);
                        isTouched[i] = false;
                        id[i] = pointerId;
                        touchEventsBuffer.add(touchEvent);
                        //Log.d("PAD-TEST", "ACTION_OUT");
                        break;

                    case MotionEvent.ACTION_MOVE:
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = TouchEvent.TOUCH_DRAGGED;

                        touchEvent.dragDuration = (System.nanoTime() - startDragging) / 1000000; // in millis
                        int currentX = (int) (event.getX(i) * scaleX);
                        int currentY = (int) (event.getY(i) * scaleY);
                        int offsetX = currentX - initialX;
                        int offsetY = currentY - initialY;
                        if (Math.abs(offsetX) > slop) {
                            if (offsetX > TouchEvent.DEFAULT_THRESHOLD) {
                                touchEvent.dragDirection = TouchEvent.DRAG_RIGHT;
                                Log.d("PAD-TEST", "ACTION_MOVE_RIGHT");
                                //Log.d("SWIPE-TEST", "Drag right");
                            } else if (offsetX < -TouchEvent.DEFAULT_THRESHOLD) {
                                touchEvent.dragDirection = TouchEvent.DRAG_LEFT;
                                Log.d("PAD-TEST", "ACTION_MOVE_LEFT");
                                //Log.d("SWIPE-TEST", "Drag left");
                            }
                        }
                        if (Math.abs(offsetY) > slop) {
                            if (offsetY > TouchEvent.DEFAULT_THRESHOLD) {
                                touchEvent.dragDirection = TouchEvent.DRAG_DOWN;
                                //Log.d("SWIPE-TEST", "Drag down");
                                Log.d("PAD-TEST", "ACTION_MOVE_DOWN");
                            } else if (offsetY < -TouchEvent.DEFAULT_THRESHOLD) {
                                touchEvent.dragDirection = TouchEvent.DRAG_UP;
                                Log.d("PAD-TEST", "ACTION_MOVE_UP");
                            }
                        }

                        touchEvent.pointer = pointerId;
                        touchEvent.x = touchX[i] = (int) (event.getX(i) * scaleX);
                        touchEvent.y = touchY[i] = (int) (event.getY(i) * scaleY);
                        isTouched[i] = true;
                        id[i] = pointerId;
                        touchEventsBuffer.add(touchEvent);
                        //Log.d("PAD-TEST", "ACTION_MOVE. P" + i + " " + isTouched[i] + " (" + touchX[i] + "," + touchY[i] + ")");

                        break;
                }
            }
            return true;
        }
    }

    public boolean isTouchDown(int pointer) {
        synchronized (this) {
            int index = getIndex(pointer);
            if (index < 0 || index >= MAX_TOUCHPOINTS)
                return false;
            else
                return isTouched[index];
        }
    }

    public int getTouchX(int pointer) {
        synchronized (this) {
            int index = getIndex(pointer);
            if (index < 0 || index >= MAX_TOUCHPOINTS)
                return 0;
            else
                return touchX[index];
        }
    }

    public int getTouchY(int pointer) {
        synchronized (this) {
            int index = getIndex(pointer);
            if (index < 0 || index >= MAX_TOUCHPOINTS)
                return 0;
            else
                return touchY[index];
        }
    }

    public List<TouchEvent> getTouchEvents() {
        synchronized (this) {
            int len = touchEvents.size();
            for (int i = 0; i < len; i++)
                touchEventPool.free(touchEvents.get(i));
            touchEvents.clear();
            touchEvents.addAll(touchEventsBuffer);
            touchEventsBuffer.clear();
            return touchEvents;
        }
    }
    
    // returns the index for a given pointerId or -1 if no index.
    private int getIndex(int pointerId) {
        for (int i = 0; i < MAX_TOUCHPOINTS; i++) {
            if (id[i] == pointerId) {
                return i;
            }
        }
        return -1;
    }
}
