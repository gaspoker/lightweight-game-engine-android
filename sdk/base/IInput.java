package sdk.base;

import java.util.List;

public interface IInput {
    class KeyEvent {
        public static final int KEY_DOWN = 0;
        public static final int KEY_UP = 1;

        public int type;
        public int keyCode;
        public char keyChar;

        public String toString() {
            StringBuilder builder = new StringBuilder();
            if (type == KEY_DOWN)
                builder.append("key down, ");
            else
                builder.append("key up, ");
            builder.append(keyCode);
            builder.append(",");
            builder.append(keyChar);
            return builder.toString();
        }
    }

    class TouchEvent {
        // Type
        public static final int TOUCH_DOWN = 0;
        public static final int TOUCH_UP = 1;
        public static final int TOUCH_DRAGGED = 2;
        public static final int TOUCH_OUT = 3;

        // Direction (Swipe)
        public static final int DEFAULT_THRESHOLD = 20;// Better to be something in dps.
        public static final int DRAG_NONE = 0;
        public static final int DRAG_DOWN = 1;
        public static final int DRAG_UP = 2;
        public static final int DRAG_LEFT = 3;
        public static final int DRAG_RIGHT = 4;

        public int type;
        public int dragDirection;
        public long dragDuration;
        public int x, y;
        public int pointer;

        public String toString() {
            StringBuilder builder = new StringBuilder();

            // Type
            if (type == TOUCH_DOWN)
                builder.append("touch down, ");
            else if (type == TOUCH_UP)
                builder.append("touch up, ");
            else if (type == TOUCH_DRAGGED)
                builder.append("touch dragged, ");
            else
                builder.append("touch out, ");

            if (dragDirection == DRAG_DOWN)
                builder.append("drag down, ");
            else if (dragDirection == DRAG_UP)
                builder.append("drag up, ");
            else if (dragDirection == DRAG_LEFT)
                builder.append("drag left, ");
            else if (dragDirection == DRAG_RIGHT)
                builder.append("drag right, ");
            else
                builder.append("no drag, ");

            builder.append(pointer);
            builder.append(",");
            builder.append(x);
            builder.append(",");
            builder.append(y);
            return builder.toString();
        }
    }


    public boolean isKeyPressed(int keyCode);

    public boolean isTouchDown(int pointer);

    public int getTouchX(int pointer);

    public int getTouchY(int pointer);

    public float getAccelX();

    public float getAccelY();

    public float getAccelZ();

    public List<KeyEvent> getKeyEvents();

    public List<TouchEvent> getTouchEvents();
}
