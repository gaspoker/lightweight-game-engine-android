package sdk.base;

import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import sdk.base.IInput.TouchEvent;
import sdk.base.Pool.PoolObjectFactory;

public class SingleTouchHandler implements ITouchHandler {
    boolean isTouched;
    int touchX;
    int touchY;
    Pool<TouchEvent> touchEventPool;
    List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();
    List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();
    float scaleX;
    float scaleY;
    final float slop;
    int initialX = 0;
    int initialY = 0;
    long startDragging;

    public SingleTouchHandler(View view, float scaleX, float scaleY) {
        PoolObjectFactory<TouchEvent> factory = new PoolObjectFactory<TouchEvent>() {
            public TouchEvent createObject() {
                return new TouchEvent();
            }            
        };
        this.touchEventPool = new Pool<TouchEvent>(factory, 100);

        this.slop = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.startDragging = 0;

        view.setOnTouchListener(this);

        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    public boolean onTouch(View v, MotionEvent event) {
        synchronized(this) {
            TouchEvent touchEvent = touchEventPool.newObject();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touchEvent.type = TouchEvent.TOUCH_DOWN;
                    touchEvent.dragDirection = TouchEvent.DRAG_NONE;
                    touchEvent.dragDuration = 0;
                    isTouched = true;
                    initialX = (int) event.getX();
                    initialY = (int) event.getY();
                    startDragging = System.nanoTime();
                    break;

                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    touchEvent.type = TouchEvent.TOUCH_UP;
                    touchEvent.dragDirection = TouchEvent.DRAG_NONE;
                    touchEvent.dragDuration = (System.nanoTime() - startDragging) / 1000000; // in millis
                    isTouched = false;
                    break;

                case MotionEvent.ACTION_HOVER_EXIT:
                case MotionEvent.ACTION_OUTSIDE:
                    touchEvent.type = TouchEvent.TOUCH_OUT;
                    touchEvent.dragDirection = TouchEvent.DRAG_NONE;
                    touchEvent.dragDuration = (System.nanoTime() - startDragging) / 1000000; // in millis
                    isTouched = false;
                    break;

                case MotionEvent.ACTION_MOVE:
                    touchEvent.type = TouchEvent.TOUCH_DRAGGED;

                    touchEvent.dragDuration = (System.nanoTime() - startDragging) / 1000000; // in millis
                    int currentX = (int) (event.getX() * scaleX);
                    int currentY = (int) (event.getY() * scaleY);
                    int offsetX = currentX - initialX;
                    int offsetY = currentY - initialY;
                    if (Math.abs(offsetX) > slop) {
                        if (offsetX > TouchEvent.DEFAULT_THRESHOLD) {
                            touchEvent.dragDirection = TouchEvent.DRAG_RIGHT;
                        } else if (offsetX < -TouchEvent.DEFAULT_THRESHOLD) {
                            touchEvent.dragDirection = TouchEvent.DRAG_LEFT;
                        }
                    }
                    if (Math.abs(offsetY) > slop) {
                        if (offsetY > TouchEvent.DEFAULT_THRESHOLD) {
                            touchEvent.dragDirection = TouchEvent.DRAG_DOWN;
                        } else if (offsetY < -TouchEvent.DEFAULT_THRESHOLD) {
                            touchEvent.dragDirection = TouchEvent.DRAG_UP;
                        }
                    }

                    isTouched = true;
                    break;
            }
            
            touchEvent.x = touchX = (int)(event.getX() * scaleX);
            touchEvent.y = touchY = (int)(event.getY() * scaleY);
            touchEventsBuffer.add(touchEvent);                        
            
            return true;
        }
    }

    public boolean isTouchDown(int pointer) {
        synchronized(this) {
            if(pointer == 0)
                return isTouched;
            else
                return false;
        }
    }

    public int getTouchX(int pointer) {
        synchronized(this) {
            return touchX;
        }
    }

    public int getTouchY(int pointer) {
        synchronized(this) {
            return touchY;
        }
    }

    public List<TouchEvent> getTouchEvents() {
        synchronized(this) {     
            int len = touchEvents.size();
            for( int i = 0; i < len; i++ )
                touchEventPool.free(touchEvents.get(i));
            touchEvents.clear();
            touchEvents.addAll(touchEventsBuffer);
            touchEventsBuffer.clear();
            return touchEvents;
        }
    }

}
