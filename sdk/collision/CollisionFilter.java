package sdk.collision;

import org.json.JSONException;
import org.json.JSONObject;

public class CollisionFilter {
    public int group;
    public int category;
    public int mask;

    public CollisionFilter(int group, int category, int mask) {
        this.group = group;
        this.category = category;
        this.mask = mask;
    }

    public CollisionFilter(JSONObject json) throws JSONException {
        group = json.getInt("group");
        category = json.getInt("category");
        mask = json.getInt("mask");
    }

}