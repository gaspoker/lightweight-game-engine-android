package sdk.collision;

import sdk.core.Body;
import sdk.geometry.Bounds;

import java.util.ArrayList;
import java.util.List;

/**
 * The `Detector` feature contains methods for detecting collisions given a set of pairs.
 *
 * @class Detector
 */
public class Detector {
    Collider cm;

    public Detector(Collider cm) {
        this.cm = cm;
    }

    // BROAD PHASE
    /**
     * Finds all collisions given a list of pairs.
     * @method collisions
     * @param {pair[]} broadphasePairs
     * @param {engine} engine
     * @return {array} collisions
     */
    public List<Collision> collisions(List<Pair> broadphasePairs) {
        List<Collision> collisions = new ArrayList();
        List<Pair> pairsTable = this.cm.pairs.table;

        for (int i = 0; i < broadphasePairs.size(); i++) {
            Body bodyA = broadphasePairs.get(i).bodyA;
            Body bodyB = broadphasePairs.get(i).bodyB;

            if ((bodyA.isStatic || bodyA.isSleeping) && (bodyB.isStatic || bodyB.isSleeping))
                continue;

            if (!Detector.canCollide(bodyA.collisionFilter, bodyB.collisionFilter))
                continue;

            // MID PHASE
            if (Bounds.overlaps(bodyA.bounds, bodyB.bounds)) {
                for (int j = bodyA.parts.size() > 1 ? 1 : 0; j < bodyA.parts.size(); j++) {
                    Body partA = bodyA.parts.get(j);

                    for (int k = bodyB.parts.size() > 1 ? 1 : 0; k < bodyB.parts.size(); k++) {
                        Body partB = bodyB.parts.get(k);

                        if ((partA == bodyA && partB == bodyB) || Bounds.overlaps(partA.bounds, partB.bounds)) {
                            // find a previous collision we could reuse
                            String pairId = Pair.id(partA, partB);
                            Pair pair = Pairs.getById(pairsTable, pairId);
                            Collision previousCollision;

                            if (pair != null && pair.isActive) {
                                previousCollision = pair.collision;
                            } else {
                                previousCollision = null;
                            }

                            // NARROW PHASE
                            Collision collision = SAT.collides(partA, partB, previousCollision);

                            if (collision.collided) {
                                collisions.add(collision);
                            }
                        }
                    }
                }
            }
        }

        return collisions;
    }

    /**
     * Returns `true` if both supplied collision filters will allow a collision to occur.
     * See `body.collisionFilter` for more information.
     * @method canCollide
     * @param {} filterA
     * @param {} filterB
     * @return {bool} `true` if collision can occur
     * Muy buena explicación: http://www.iforce2d.net/b2dtut/collision-filtering
     */
    private static boolean canCollide(CollisionFilter filterA, CollisionFilter filterB) {
        if (filterA.group == filterB.group && filterA.group != 0)
            return filterA.group > 0;

        return (filterA.mask & filterB.category) != 0 && (filterB.mask & filterA.category) != 0;
    }



}
