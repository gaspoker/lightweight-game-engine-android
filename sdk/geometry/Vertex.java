package sdk.geometry;

import sdk.core.Body;

public class Vertex extends Vector {
    public boolean isInternal;
    public int index;
    public Body body;
    public Contact contact;
    Float offset;

    public Vertex() {
        super();
    }

    public Vertex(float x, float y) {
        super(x, y);
    }

    public Vertex(Vector point) {
        super(point.x, point.y);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Internal Classes
    ////////////////////////////////////////////////////////////////////////////////////////////////
    public static class Contact {
        Vertex vertex;
        float normalImpulse;
        float tangentImpulse;

        Contact(Vertex vertex, float normalImpulse, float tangentImpulse) {
            this.vertex = vertex;
            this.normalImpulse = normalImpulse;
            this.tangentImpulse = tangentImpulse;
        }

    }



}
