package sdk.geometry;

import android.support.annotation.NonNull;

/**
 * The `Vector` feature contains methods for creating and manipulating vectors.
 * Vectors are the basis of all the geometry related operations in the engine.
 * A `Vector` object is of the form `{ x: 0, y: 0 }`
 *
 * @class Vector
 */
public class Vector {
    public static float TO_RADIANS = (1f / 180f) * (float) Math.PI;
    public static float TO_DEGREES = (1f / (float) Math.PI) * 180f;
    public float x, y;

    public Vector() {
        this.x = 0f;
        this.y = 0f;
    }

    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector(Vector other) {
        this.x = other.x;
        this.y = other.y;
    }

    public Vector cpy() {
        return new Vector(x, y);
    }

    public Vector set(float x, float y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Vector set(Vector other) {
        this.x = other.x;
        this.y = other.y;
        return this;
    }

    public Vector add(float x, float y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public Vector add(Vector other) {
        this.x += other.x;
        this.y += other.y;
        return this;
    }

    public Vector sub(float x, float y) {
        this.x -= x;
        this.y -= y;
        return this;
    }

    public Vector sub(Vector other) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    public Vector mult(float scalar) {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

    public Vector div(float scalar) {
        this.x /= scalar;
        this.y /= scalar;
        return this;
    }

    public float len() {
        return (float) Math.sqrt(x * x + y * y);
    }

    public Vector nor() {
        float len = len();
        if (len != 0f) {
            this.x /= len;
            this.y /= len;
        }
        return this;
    }

    /*public float angleD() {
        float angle = (float) Math.atan2(y, x) * TO_DEGREES;
        if (angle < 0f)
            angle += 360f;
        return angle;
    }*/


    public Vector rotate(float angle) {
        float rad = angle * TO_RADIANS;
        float cos = (float) Math.cos(rad);
        float sin = (float) Math.sin(rad);

        float newX = this.x * cos - this.y * sin;
        float newY = this.x * sin + this.y * cos;

        this.x = newX;
        this.y = newY;

        return this;
    }

    public float dist(Vector other) {
        float distX = this.x - other.x;
        float distY = this.y - other.y;
        return (float) Math.sqrt(distX * distX + distY * distY);
    }

    public float dist(float x, float y) {
        float distX = this.x - x;
        float distY = this.y - y;
        return (float) Math.sqrt(distX * distX + distY * distY);
    }

    public float distSquared(Vector other) {
        float distX = this.x - other.x;
        float distY = this.y - other.y;
        return distX * distX + distY * distY;
    }

    public float distSquared(float x, float y) {
        float distX = this.x - x;
        float distY = this.y - y;
        return distX * distX + distY * distY;
    }

    /**
     * Helper macro that creates a Point
     *
     * @return Point
     */
    public static Vector create(float x, float y) {
        return new Vector(x, y);
    }

    /**
     * Adds the two vectors.
     * @method add
     * @param {vector} vectorA
     * @param {vector} vectorB
     * @param {vector} [output]
     * @return {vector} A new vector of vectorA and vectorB added
     */
    public static Vector add(Vector vectorA, Vector vectorB, Vector output) {
        output = (output != null) ? output : new Vector();
        output.x = vectorA.x + vectorB.x;
        output.y = vectorA.y + vectorB.y;
        return output;
    }


    /**
     * Divides a vector and a scalar.
     * @method div
     * @param {vector} vector
     * @param {number} scalar
     * @return {vector} A new vector divided by scalar
     */
    public static Vector div(Vector vector, float scalar) {
        Vector output = new Vector();
        output.x = vector.x / scalar;
        output.y = vector.y / scalar;
        return output;
    }

    /**
     * Returns the perpendicular vector. Set `negate` to true for the perpendicular in the opposite direction.
     * @method perp
     * @param {vector} vector
     * @param {bool} [negate=false]
     * @return {vector} The perpendicular vector
     */
    public static Vector perp(Vector vector, boolean negate) {
        float mult = negate ? -1f : 1f;
        return new Vector(mult * -vector.y, mult * vector.x);
    }

    /**
     * Negates both components of a vector such that it points in the opposite direction.
     * @method neg
     * @param {vector} vector
     * @return {vector} The negated vector
     */
    public static Vector neg(Vector vector) {
        return new Vector(-vector.x, -vector.y);
    }

    /**
     * Multiplies a vector and a scalar.
     * @method mult
     * @param {vector} vector
     * @param {number} scalar
     * @return {vector} A new vector multiplied by scalar
     */
    public static Vector mult(Vector vector, float scalar) {
        Vector output = new Vector();
        output.x = vector.x * scalar;
        output.y = vector.y * scalar;
        return output;
    }

    /**
     * Subtracts the two vectors.
     * @method sub
     * @param {vector} vectorA
     * @param {vector} vectorB
     * @param {vector} [output]
     * @return {vector} A new vector of vectorA and vectorB subtracted
     */
    public static Vector sub(Vector vectorA, Vector vectorB, Vector output) {
        output = (output != null) ? output : new Vector();
        output.x = vectorA.x - vectorB.x;
        output.y = vectorA.y - vectorB.y;
        return output;
    }

    /**
     * Returns the angle between the vector and `vectorB - vectorA` and the x-axis in radians.
     * @method angle
     * @param {vector} vectorA
     * @param {vector} vectorB
     * @return {number} The angle in radians
     */
    public static float angle(Vector vectorA, Vector vectorB) {
        return (float) Math.atan2(vectorB.y - vectorA.y, vectorB.x - vectorA.x);
    }

    /**
     * Returns the cross-product of two vectors.
     * @method cross
     * @param {vector} vectorA
     * @param {vector} vectorB
     * @return {number} The cross product of the two vectors
     */
    public static float cross(Vector vectorA, Vector vectorB) {
        return (vectorA.x * vectorB.y) - (vectorA.y * vectorB.x);
    }

    /**
     * Returns the cross-product of three vectors.
     * @method cross3
     * @param {vector} vectorA
     * @param {vector} vectorB
     * @param {vector} vectorC
     * @return {number} The cross product of the three vectors
     */
    public static float cross3(Vector vectorA, Vector vectorB, Vector vectorC) {
        return (vectorB.x - vectorA.x) * (vectorC.y - vectorA.y) - (vectorB.y - vectorA.y) * (vectorC.x - vectorA.x);
    }

    /**
     * Returns the magnitude (length) of a vector.
     * @method magnitude
     * @param {vector} vector
     * @return {number} The magnitude of the vector
     */
    public static float magnitude(Vector vector) {
        return (float) Math.sqrt((vector.x * vector.x) + (vector.y * vector.y));
    }

    /**
     * Returns the magnitude (length) of a vector (therefore saving a `sqrt` operation).
     * @method magnitudeSquared
     * @param {vector} vector
     * @return {number} The squared magnitude of the vector
     */
    public static float magnitudeSquared(Vector vector) {
        return (vector.x * vector.x) + (vector.y * vector.y);
    }

    /**
     * Rotates the vector about (0, 0) by specified angle.
     * @method rotate
     * @param {vector} vector
     * @param {number} angle
     * @param {vector} [output]
     * @return {vector} The vector rotated about (0, 0)
     */
    public static Vector rotate(Vector vector, float angle, Vector output) {
        output = (output != null) ? output : new Vector();
        float cos = (float) Math.cos(angle);
        float sin = (float) Math.sin(angle);
        float x = vector.x * cos - vector.y * sin;
        output.y = vector.x * sin + vector.y * cos;
        output.x = x;
        return output;
    }

    /**
     * Rotates the vector about a specified point by specified angle.
     * @method rotateAbout
     * @param {vector} vector
     * @param {number} angle
     * @param {vector} point
     * @param {vector} [output]
     * @return {vector} A new vector rotated about the point
     */
    public static Vector rotateAbout(Vector vector, float angle, Vector point, Vector output) {
        output = (output != null) ? output : new Vector();
        float cos = (float) Math.cos(angle);
        float sin = (float) Math.sin(angle);
        float x = point.x + ((vector.x - point.x) * cos - (vector.y - point.y) * sin);
        output.y = point.y + ((vector.x - point.x) * sin + (vector.y - point.y) * cos);
        output.x = x;
        return output;
    }

    /**
     * Normalises a vector (such that its magnitude is `1`).
     * @method normalise
     * @param {vector} vector
     * @return {vector} A new vector normalised
     */
    public static Vector normalise(Vector vector) {
        Vector output = new Vector();
        float magnitude = Vector.magnitude(vector);
        if (magnitude == 0f) {
            return output;
        }
        output.y = vector.y / magnitude;
        output.x = vector.x / magnitude;
        return output;
    }

    /**
     * Returns the dot-product of two vectors.
     * @method dot
     * @param {vector} vectorA
     * @param {vector} vectorB
     * @return {number} The dot product of the two vectors
     */
    public static float dot(Vector vectorA, Vector vectorB) {
        return (vectorA.x * vectorB.x) + (vectorA.y * vectorB.y);
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + x + ", " + y + "]";
    }
}
