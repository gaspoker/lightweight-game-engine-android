package sdk.geometry;

import java.util.ArrayList;
import java.util.List;

/**
 * The `Axes` feature contains methods for creating and manipulating sets of axes.
 *
 * @class Axes
 */
public class Axes {

    /**
     * Creates a new set of axes from the given vertices.
     * @method fromVertices
     * @param {vertices} vertices
     * @return {axes} A new axes from the given vertices
     */
    public static List<Vector> fromVertices(List<Vertex> vertices) {
        List<Vector> axes = new ArrayList<>();

        // find the unique axes, using edge normal gradients
        for (int i = 0; i < vertices.size(); i++) {
            int j = (i + 1) % vertices.size();
            Vector normal = Vector.normalise(new Vector(vertices.get(j).y - vertices.get(i).y, vertices.get(i).x - vertices.get(j).x));
            float gradient = (normal.y == 0f) ? Float.POSITIVE_INFINITY : (normal.x / normal.y);

            // limit precision
            //String gradientStr = Double.toString(Math.round(gradient) / 1000);
            axes.add(normal);
        }

        return axes;
    }

    /**
     * Rotates a set of axes by the given angle.
     * @method rotate
     * @param {axes} axes
     * @param {number} angle
     */
    public static void rotate(List<Vector> axes, float angle) {
        if (angle != 0) {
            float cos = (float) Math.cos(angle);
            float sin = (float) Math.sin(angle);

            for (int i = 0; i < axes.size(); i++) {
                Vector axis = axes.get(i);
                float aux = axis.x * cos - axis.y * sin;
                axis.y = axis.x * sin + axis.y * cos;
                axis.x = aux;
            }

        } // if
    }

}
