package sdk.geometry;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * The `Bounds` feature contains methods for creating and manipulating axis-aligned bounding boxes (AABB).
 *
 * @class Bounds
 */
public class Bounds {
    public Vertex min;
    public Vertex max;

    public Bounds(float minX, float minY, float maxX, float maxY) {
        this.min = new Vertex(minX, minY);
        this.max = new Vertex(maxX, maxY);
    }

    public Bounds() {
        this.min = new Vertex(0f,0f);
        this.max = new Vertex(0f,0f);
    }

    public Bounds(List<Vertex> vertices) {
        this.min = new Vertex(0f,0f);
        this.max = new Vertex(0f,0f);
        update(vertices, null);
    }

    /**
     * Updates bounds using the given vertices and extends the bounds given a velocity.
     * @method update
     * @param {bounds} bounds
     * @param {vertices} vertices
     * @param {vector} velocity
     */
    public void update(List<Vertex> vertices, Vector velocity) {
        this.min.x = Float.POSITIVE_INFINITY;
        this.max.x = Float.NEGATIVE_INFINITY;
        this.min.y = Float.POSITIVE_INFINITY;
        this.max.y = Float.NEGATIVE_INFINITY;

        //for (Vertex vertex : vertices) {
        for (int i = 0; i < vertices.size(); i++) {
            Vertex vertex = vertices.get(i);

            if (vertex.x > this.max.x) this.max.x = vertex.x;
            if (vertex.x < this.min.x) this.min.x = vertex.x;
            if (vertex.y > this.max.y) this.max.y = vertex.y;
            if (vertex.y < this.min.y) this.min.y = vertex.y;
        }

        if (velocity != null) {
            if (velocity.x > 0f) {
                this.max.x += velocity.x;
            } else {
                this.min.x += velocity.x;
            }

            if (velocity.y > 0f) {
                this.max.y += velocity.y;
            } else {
                this.min.y += velocity.y;
            }
        }
    }

    /**
     * Returns true if the bounds contains the given point.
     * @method contains
     * @param {bounds} bounds
     * @param {vector} point
     * @return {boolean} True if the bounds contain the point, otherwise false
     */
    public boolean contains(Vector point) {
        return point.x >= this.min.x && point.x <= this.max.x
                && point.y >= this.min.y && point.y <= this.max.y;
    }

    /**
     * Returns true if the two bounds intersect.
     * @method overlaps
     * @param {bounds} boundsA
     * @param {bounds} boundsB
     * @return {boolean} True if the bounds overlap, otherwise false
     */
    public static boolean overlaps(Bounds boundsA, Bounds boundsB) {
        return (boundsA.min.x <= boundsB.max.x && boundsA.max.x >= boundsB.min.x
                && boundsA.max.y >= boundsB.min.y && boundsA.min.y <= boundsB.max.y);
    }

    /**
     * Translates the bounds by the given vector.
     * @method translate
     * @param {bounds} bounds
     * @param {vector} vector
     */
    public void translate(Vector vector) {
        this.min.x += vector.x;
        this.max.x += vector.x;
        this.min.y += vector.y;
        this.max.y += vector.y;
    }

    /**
     * Shifts the bounds to the given position.
     * @method shift
     * @param {bounds} bounds
     * @param {vector} position
     */
    public void shift(Vector position) {
        float deltaX = this.max.x - this.min.x;
        float deltaY = this.max.y - this.min.y;

        this.min.x = position.x;
        this.max.x = position.x + deltaX;
        this.min.y = position.y;
        this.max.y = position.y + deltaY;
    }


    @NonNull
    @Override
    public String toString() {
        return "MIN x: " + min.x + ", y: " + min.y + " | MAX x: " + max.x + ", y: " + max.y;
    }
}
