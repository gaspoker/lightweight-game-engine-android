package sdk.canvas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;
import sdk.base.IAudio;
import sdk.base.IFileIO;
import sdk.base.IInput;
import sdk.base.Audio;
import sdk.base.FileIO;
import sdk.base.Input;
import sdk.base.IGame;
import sdk.base.IGraphics;
import sdk.collision.Collider;
import sdk.core.Scene;

public abstract class CanvasGame extends Activity implements IGame {
    CanvasFastRenderView renderView;
    IGraphics graphics;
    IAudio audio;
    IInput input;
    IFileIO fileIO;
    Scene scene;
    WakeLock wakeLock;
    float width;
    float height;

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        int frameBufferWidth = isLandscape ? 480 : 320;
        int frameBufferHeight = isLandscape ? 320 : 480;
        Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
                frameBufferHeight, Config.RGB_565);
        
        float scaleX = (float) frameBufferWidth
                / getWindowManager().getDefaultDisplay().getWidth();
        float scaleY = (float) frameBufferHeight
                / getWindowManager().getDefaultDisplay().getHeight();

        renderView = new CanvasFastRenderView(this, frameBuffer);

        graphics = new CanvasGraphics(getAssets(), frameBuffer);
        fileIO = new FileIO(this);
        audio = new Audio(this);
        input = new Input(this, renderView, scaleX, scaleY);
        scene = getMainScene();
        setContentView(renderView);
        
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "GLGame");
    }

    @Override
    public void onResume() {
        super.onResume();
        wakeLock.acquire();
        scene.resume();
        renderView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        wakeLock.release();
        renderView.pause();
        scene.pause();

        if (isFinishing())
            scene.dispose();
    }

    public IInput getInput() {
        return input;
    }

    public IFileIO getFileIO() {
        return fileIO;
    }

    public IGraphics getGraphics() {
        return graphics;
    }

    public IAudio getAudio() {
        return audio;
    }

    public void setScene(Scene scene) {
        if (scene == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.scene.pause();
        this.scene.dispose();
        scene.resume();
        scene.update(0);
        this.scene = scene;
    }

    public Scene getScene() {
        return this.scene;
    }

}
