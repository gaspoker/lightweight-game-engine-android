package sdk.core;

import sdk.geometry.Bounds;
import sdk.geometry.Vector;
import sdk.geometry.Vertex;
import sdk.geometry.Vertices;

import java.util.List;

/**
 * The `Bodies` feature contains factory methods for creating rigid body models
 * with commonly used body configurations (such as rectangles, circles and other polygons).
 *
 * @class Bodies
 */
public class Bodies {

    /**
     * Creates a new rigid body model with a rectangle hull.
     * The options parameter is an object that specifies any properties you wish to override the defaults.
     * See the properties section of the `Body` feature for detailed information on what you can pass via the `options` object.
     * @method rectangle
     * @param {number} x
     * @param {number} y
     * @param {number} width
     * @param {number} height
     * @param {object} [options]
     * @return {body} A new rectangle body
     */
//    public static Body rectangle(float x, float y, float width, float height) {
//        //Body body = new Body();
//        /*body.label = "Rectangle Body";
//        body.position = new Vector(x, y);
//        body.vertices = Vertices.fromPath("L 0 0 L " + width + " 0 L " + width + " " + height + " L 0 " + height, body);*/
//
//        /*if (options.chamfer) {
//            var chamfer = options.chamfer;
//            rectangle.vertices = Vertices.chamfer(rectangle.vertices, chamfer.radius,
//                    chamfer.quality, chamfer.qualityMin, chamfer.qualityMax);
//            delete options.chamfer;
//        }*/
//
//        return body;
//    }


    /**
     * Takes an array of Body objects and flags all internal edges (coincident parts) based on the maxDistance
     * value. The array is changed in-place and returned, so you can pass this function a `Body.parts` property.
     *
     * @method flagCoincidentParts
     * @param {body[]} parts - The Body parts, or array of bodies, to flag.
     * @param {number} [maxDistance=5]
     * @return {body[]} The modified `parts` parameter.
     */
    static List<Body> flagCoincidentParts(List<Body> parts) {
        float maxDistance = 5;
        return flagCoincidentParts(parts, maxDistance);
    }

    private static List<Body> flagCoincidentParts(List<Body> parts, float maxDistance) {
        for (int i = 0; i < parts.size(); i++) {
            Body partA = parts.get(i);

            for (int j = i + 1; j < parts.size(); j++) {
                Body partB = parts.get(j);

                if (Bounds.overlaps(partA.bounds, partB.bounds)) {
                    List<Vertex> pav = partA.vertices;
                    List<Vertex> pbv = partB.vertices;

                    // iterate vertices of both parts
                    for (int k = 0; k < partA.vertices.size(); k++) {
                        for (int z = 0; z < partB.vertices.size(); z++) {
                            // find distances between the vertices
                            float da = Vector.magnitudeSquared(Vector.sub(pav.get((k + 1) % pav.size()), pbv.get(z), null));
                            float db = Vector.magnitudeSquared(Vector.sub(pav.get(k), pbv.get((z + 1) % pbv.size()), null));

                            // if both vertices are very close, consider the edge concident (internal)
                            if (da < maxDistance && db < maxDistance) {
                                pav.get(k).isInternal = true;
                                pbv.get(z).isInternal = true;
                            }

                        } // for
                    } // for
                } // if

            } // for
        } // for

        return parts;
    }


}
