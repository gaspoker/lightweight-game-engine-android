package sdk.core;

import sdk.geometry.Rectangle;
import sdk.geometry.Vector;
import sdk.core.Body.BodyCollisionListener;
import sdk.core.Body.BodySimpleCollisionListener;
import org.json.JSONObject;

public class GameObject {
    protected Scene scene;

    // Body
    protected Body body;

    // Game Object
    public float width, height;
    public float alpha;
    private float halfWidth, halfHeight;
    public String type;
    protected int state;
    public String name;
    private boolean active;
    public boolean visible;
    protected boolean ignoreDestroy;
    protected float originX, originY;
    protected float displayOriginX, displayOriginY;

    public void setCollisionListener(BodyCollisionListener listener) {
        //this.body.listener = listener;
        this.body.setCollisionListener(listener);
    }

    public void setSimpleCollisionListener(BodySimpleCollisionListener listener) {
        //this.body.simpleListener = listener;
        this.body.setSimpleCollisionListener(listener);
    }

    private void initProperties(Scene scene, float width, float height) {
        this.scene = scene;
        this.width = width;
        this.height = height;
        this.halfWidth = Math.abs(this.width / 2f);
        this.halfHeight = Math.abs(this.height / 2f);
        this.alpha = 1f;
        this.state = 0;
        this.name = "";
        this.type = "";
        this.active = true;
        this.visible = true;
        this.ignoreDestroy = false;
        this.originX = 0.5f;
        this.originY = 0.5f;
        this.displayOriginX = 0f;
        this.displayOriginY = 0f;
    }

    public GameObject(Scene scene, Rectangle rect) {
        this(scene, rect.lowerLeft.x, rect.lowerLeft.y, rect.width, rect.height);
    }

    public GameObject(Scene scene, float x, float y, float width, float height) {
        initProperties(scene, width, height);

        // Body
        this.body = new Body(x, y, width, height);
        this.body.setGameObject(this);

        // Origin
        float cx = this.body.centerOffset.x;
        float cy = this.body.centerOffset.y;
        this.setOrigin(cx / this.getDisplayWidth(), cy / this.getDisplayHeight());

        this.setPosition(x, y);

        // Collider
        this.scene.collider.addBody(this.body);
    }

    public GameObject(Scene scene, float x, float y, float width, float height, JSONObject shapes, String shapeName) {
        initProperties(scene, width, height);

        // Body
        this.body = new Body(x, y, shapes, shapeName);
        this.body.setGameObject(this);

        // Origin
        float cx = this.body.centerOffset.x;
        float cy = this.body.centerOffset.y;
        this.setOrigin(cx / this.getDisplayWidth(), cy / this.getDisplayHeight());

        this.setPosition(x, y);

        // Add GameObject body to Collider
        this.scene.collider.addBody(this.body);
    }

    private void setBodyPosition(float x, float y) {

    }

    public void setX(float x) {
        Vector bodyPosition = new Vector(x - this.halfWidth + this.displayOriginX, this.body.position.y);
        this.body.setPosition(bodyPosition);
    }

    public float getX() {
        return this.body.position.x + this.halfWidth - this.displayOriginX;
    }

    public void incX(float inc) {
        this.setX(this.getX() + inc);
    }

    public void setY(float y) {
        Vector bodyPosition = new Vector(this.body.position.x, y - this.halfHeight + this.displayOriginY);
        this.body.setPosition(bodyPosition);
    }

    public float getY() {
        return this.body.position.y + this.halfHeight - this.displayOriginY;
    }

    public void incY(float inc) {
        this.setY(this.getY() + inc);
    }

    public void setPosition(Vector v) {
        setPosition(v.x, v.y);
    }

    public void setPosition(float x, float y) {
        Vector bodyPosition = new Vector(x - this.halfWidth + this.displayOriginX, y - this.halfHeight + this.displayOriginY);
        this.body.setPosition(bodyPosition);
    }

    public Vector getPosition() {
        return new Vector(getX(), getY());
    }

    public void setTopLeftPosition(float x, float y) {
        Vector bodyPosition = new Vector(x + this.halfWidth + this.displayOriginX, y + this.halfHeight + this.displayOriginY);
        this.body.setPosition(bodyPosition);
    }

    public void setLeftPosition(float x) {
        Vector bodyPosition = new Vector(x + this.halfWidth + this.displayOriginX, this.body.position.y);
        this.body.setPosition(bodyPosition);
    }

    public void setRightPosition(float x) {
        Vector bodyPosition = new Vector(x + this.halfWidth + this.displayOriginX + this.width, this.body.position.y);
        this.body.setPosition(bodyPosition);
    }


    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public void setAngle(float angle) {
        this.body.setAngle(angle);
    }

    public float getAngle() {
        return this.body.angle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.body.setLabel(name);
    }

    /**
     * The displayed width of this Game Object.
     *
     * This value takes into account the scale factor.
     *
     * Setting this value will adjust the Game Object's scale property.
     *
     * @name GameObjects.Components.Size#displayWidth
     * @type {number}
     * @since 3.0.0
     */
    public float getDisplayWidth() {
        return Math.abs(this.body.scale.x * this.width);
    }

    public void setDisplayWidth(float width) {
        float scaleX = width / this.width;
        float scaleY = this.body.scale.y;
        this.body.scale(scaleX, scaleY, null);
    }

    /**
     * The displayed height of this Game Object.
     *
     * This value takes into account the scale factor.
     *
     * Setting this value will adjust the Game Object's scale property.
     *
     * @name GameObjects.Components.Size#displayHeight
     * @type {number}
     * @since 3.0.0
     */
    public float getDisplayHeight() {
        return Math.abs(this.body.scale.y * this.height);
    }

    public void setDisplayHeight(float height) {
        float scaleX = this.body.scale.x;
        float scaleY = height / this.height;
        this.body.scale(scaleX, scaleY, null);
    }

    /**
     * Sets the display size of this Game Object.
     *
     * Calling this will adjust the scale.
     *
     * @method GameObjects.Components.Size#setDisplaySize
     * @since 3.0.0
     *
     * @param {number} width - The width of this Game Object.
     * @param {number} height - The height of this Game Object.
     *
     * @return {this} This Game Object instance.
     */
    public void setDisplaySize(float width, float height) {
        this.setDisplayWidth(width);
        this.setDisplayHeight(height);
    }

    public float getTop() {
        return this.body.position.y - this.displayOriginY;
    }

    public float getLeft() {
        return this.body.position.x - this.displayOriginX;
    }

    public float getBottom() {
        return this.body.position.y - this.displayOriginY + this.height;
    }

    public float getRight() {
        return this.body.position.x - this.displayOriginX + this.width;
    }

    public void setStatic(boolean value) {
        this.body.setStatic(value);
    }

    public boolean isStatic() {
        return this.body.isStatic;
    }

    public void setSensor(boolean value) {
        this.body.isSensor = value;
    }

    public boolean isSensor() {
        return this.body.isSensor;
    }



    /**
     * Sets the origin of this Game Object.
     *
     * The values are given in the range 0 to 1.
     *
     * @method GameObjects.Components.Origin#setOrigin
     * @since 3.0.0
     *
     * @param {number} [x=0.5] - The horizontal origin value.
     * @param {number} [y=x] - The vertical origin value. If not defined it will be set to the value of `x`
     *
     * @return {this} This Game Object instance.
     */
    public void setOrigin() {
        this.originX = 0.5f;
        this.originY = 0.5f;

        this.updateDisplayOrigin();
    }

    public void setOrigin(float x, float y) {
        this.originX = x;
        this.originY = y;

        this.updateDisplayOrigin();
    }

    public float getOriginX() {
        return originX;
    }

    public float getOriginY() {
        return originY;
    }

    //    public void set(float x, float y, float width, float height) {
//        this.set(x, y);
//        this.bounds.set(x-width/2, y-height/2, width, height);
//    }




    /**
     * The horizontal display origin of this Game Object.
     * The origin is a normalized value between 0 and 1.
     * The displayOrigin is a pixel value, based on the size of the Game Object combined with the origin.
     *
     * @name GameObjects.Components.Origin#displayOriginX
     * @type {number}
     * @since 3.0.0
     */
    public float getDisplayOriginX() {
        return this.displayOriginX;
    }

    public void setDisplayOriginX(float value) {
        this.displayOriginX = value;
        this.originX = value / this.width;
    }

    /**
     * The vertical display origin of this Game Object.
     * The origin is a normalized value between 0 and 1.
     * The displayOrigin is a pixel value, based on the size of the Game Object combined with the origin.
     *
     * @name GameObjects.Components.Origin#displayOriginY
     * @type {number}
     * @since 3.0.0
     */
    public float getDisplayOriginY() {
        return this.displayOriginY;
    }

    public void setDisplayOriginY(float value) {
        this.displayOriginY = value;
        this.originY = value / this.height;
    }

    /**
     * Sets the display origin of this Game Object.
     * The difference between this and setting the origin is that you can use pixel values for setting the display origin.
     *
     * @method GameObjects.Components.Origin#setDisplayOrigin
     * @since 3.0.0
     *
     * @param {number} [x=0] - The horizontal display origin value.
     * @param {number} [y=x] - The vertical display origin value. If not defined it will be set to the value of `x`
     *
     * @return {this} This Game Object instance.
     */
    public void setDisplayOrigin() {
        this.displayOriginX = 0f;
        this.displayOriginY = 0f;
    }

    public void setDisplayOrigin(float x, float y) {
        this.displayOriginX = x;
        this.displayOriginY = y;
    }

    /**
     * Updates the Display Origin cached values internally stored on this Game Object.
     * You don't usually call this directly, but it is exposed for edge-cases where you may.
     *
     * @method GameObjects.Components.Origin#updateDisplayOrigin
     * @since 3.0.0
     *
     * @return {this} This Game Object instance.
     */
    public void updateDisplayOrigin() {
        this.displayOriginX = this.originX * this.width;
        this.displayOriginY = this.originY * this.height;
    }


    public void setVelocity(float velX, float velY) {
        this.body.setVelocity(new Vector(velX, velY));
    }

    public void setVelocityX(float vel) {
        this.body.setVelocity(new Vector(vel, this.body.velocity.y));
    }

    public void setVelocityY(float vel) {
        this.body.setVelocity(new Vector(this.body.velocity.x, vel));
    }

    public float getVelocityX() {
        return this.body.velocity.x;
    }

    public float getVelocityY() {
        return this.body.velocity.y;
    }



    public boolean isActive() {
        return active;
    }

    /**
     * Sets the `active` property of this Game Object and returns this Game Object for further chaining.
     * A Game Object with its `active` property set to `true` will be updated by the Scenes UpdateList.
     *
     * @method GameObjects.GameObject#setActive
     * @since 3.0.0
     *
     * @param {boolean} value - True if this Game Object should be set as active, false if not.
     *
     * @return {this} This GameObject.
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Sets the visibility of this Game Object.
     *
     * An invisible Game Object will skip rendering, but will still process update logic.
     *
     * @method GameObjects.Components.Visible#setVisible
     * @since 3.0.0
     *
     * @param {boolean} value - The visible state of the Game Object.
     *
     * @return {this} This Game Object instance.
     */
    public void setVisible(boolean value) {
        visible = value;

        if (value) alpha = 1f; else alpha = 0f;
    }

    public void destroy() {
        setPosition(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);

        // Remove GameObject body from Collider
        scene.collider.removeBody(this.body);

        setActive(false);
        setVisible(false);
    }

    protected void finalize() throws Throwable {
        super.finalize();
        destroy();
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }
}
