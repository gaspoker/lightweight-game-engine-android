package sdk.core;

import android.graphics.Color;

import sdk.opengl.GLAnimation;
import sdk.opengl.GLTextureRegion;

import org.json.JSONObject;

public class Sprite extends GameObject {

    private GLTextureRegion texture;
    private GLAnimation anims;
    public int tint;
    private float sumTime = 0f;

    private void initProperties(GLTextureRegion texture, GLAnimation anims) {
        this.type = "Sprite";
        this.texture = texture;
        this.anims = anims;
        this.tint = Color.WHITE;
    }

    public Sprite(Scene scene, float x, float y, float width, float height, GLAnimation anims) {
        super(scene, x, y, width, height);
        initProperties(null, anims);
    }

    public Sprite(Scene scene, float x, float y, float width, float height, GLTextureRegion texture) {
        super(scene, x, y, width, height);
        initProperties(texture, null);
    }

    public Sprite(Scene scene, float x, float y, float width, float height, GLTextureRegion texture, JSONObject shapes, String shapeName) {
        super(scene, x, y, width, height, shapes, shapeName);
        initProperties(texture, null);
    }

    public int getTint() {
        return tint;
    }

    public void setTint(int tint) {
        this.tint = tint;
    }

    public void setTexture(GLTextureRegion texture) {
        this.texture = texture;
    }

    public GLTextureRegion getTexture() {
        return texture;
    }

    //public GLTextureRegion getTexture(float deltaTime, int mode) {
    public GLTextureRegion getTexture(float deltaTime) {
        sumTime += deltaTime;
        //return anims.getKeyFrame(sumTime, mode);
        return anims.getKeyFrame(sumTime);
    }

    public void resetAnims() {
        sumTime = 0f;
    }

    public GLAnimation getAnims() {
        return anims;
    }

    public void setAnims(GLAnimation anims) {
        if (this.anims != anims) this.anims = anims;
    }


    /**
     * Start playing the given animation.
     *
     * @method GameObjects.Sprite#play
     * @since 3.0.0
     *
     * @param {string} key - The string-based key of the animation to play.
     * @param {boolean} [ignoreIfPlaying=false] - If an animation is already playing then ignore this call.
     * @param {integer} [startFrame=0] - Optionally start the animation playing from this frame index.
     *
     * @return {GameObjects.Sprite} This Game Object.
     */
    /*public void play(GLAnimation anims) {
        this.anims = anims;
    }*/

}
