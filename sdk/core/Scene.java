package sdk.core;

import sdk.base.IGame;
import sdk.collision.Collider;
import sdk.collision.Collider.CollisionListener;

public abstract class Scene implements CollisionListener {
    public final IGame game;
    public Collider collider;

    public Scene(IGame game) {
        this.game = game;
        this.collider = new Collider(this, this);
    }

    public abstract void update(float deltaTime);

    public abstract void present(float deltaTime);

    public abstract void pause();

    public abstract void resume();

    public abstract void dispose();

    // Agrego objetos al Collider
    public void add() {

    }
}
