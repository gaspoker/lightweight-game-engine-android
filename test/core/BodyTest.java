package test.core;

import android.support.annotation.NonNull;
import android.util.Log;

import sdk.collision.CollisionFilter;
import sdk.collision.Grid;
import sdk.collision.Pair;
import sdk.geometry.Axes;
import sdk.geometry.Bounds;
import sdk.geometry.Vector;
import sdk.geometry.Vertex;
import sdk.geometry.Vertices;
import sdk.geometry.Vertices.ClockwiseSort;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The `Body` feature contains methods for creating and manipulating body models.
 * A `Body` is a rigid body that can be simulated by a `Engine`
 * Factories for commonly used body configurations (such as rectangles, circles and other polygons) can be found in the feature `Bodies`
 *
 * @class Body
 */
public class BodyTest {
    public final static float INERTIA_SCALE = 4f;
    public final static float NEXT_COLLIDING_GROUP_ID = 1;
    public final static float NEXT_NON_COLLIDING_GROUP_ID = -1;
    public final static float NEXT_CATEGORY = 0x0001;

    public interface BodyCollisionListener {
        void onCollideStart(Pair data);
        void onCollideActive(Pair data);
        void onCollideEnd(Pair data);
    }

    public interface BodySimpleCollisionListener {
        void onCollide(Pair data, Body self, Body other);
    }

    protected BodyCollisionListener listener;
    protected BodySimpleCollisionListener simpleListener;

    private static final AtomicInteger idGen = new AtomicInteger(0);
    public int id;
    public String type;
    public String label;
    public List<Body> parts;
    public float angle;
    public List<Vertex> vertices;
    public Vector position;
    public Vector force;
    public float torque;
    public Vector positionImpulse;
    public Vector previousPositionImpulse;
    public Vector constraintImpulse;
    public int totalContacts;
    public float speed;
    public float angularSpeed;
    public Vector velocity;
    public float angularVelocity;
    public boolean isSensor;
    public boolean isStatic;
    public boolean isSleeping;
    private float sleepCounter;
    public float motion;
    public float sleepThreshold;
    public float density;
    public float restitution;
    public float friction;
    public float frictionStatic;
    public float frictionAir;
    public CollisionFilter collisionFilter;
    public float slop;
    public float timeScale;
    //events: null,
    public Bounds bounds;
    //public float chamfer = null;
    public float circleRadius;
    public Vector positionPrev;
    public float anglePrev;
    public Body parent;
    public List<Vector> axes;
    public float area;
    public float mass;
    public float inverseMass;
    public float inertia;
    public float inverseInertia;
    public RigidBody original;
    public GameObject gameObject;
    public Grid.Region region;
    /*render: {
        visible: true,
        opacity: 1,
        sprite: {
            xOffset: 0,
            yOffset: 0
        },
        fillColor: null,            // custom  property
        fillOpacity: null,          // custom  property
        lineColor: null,            // custom  property
        lineOpacity: null,          // custom  property
        lineThickness: null         // custom  property
    },*/
    Vector scale;          // custom  property
    Vector centerOfMass;   // custom  property (float, 0 - 1)
    Vector centerOffset;   // custom  property (pixel values)
    Vector gravityScale;   // custom  property
    boolean ignoreGravity;           // custom  property
    boolean ignorePointer;           // custom  property
    /*onCollideCallback: null,        // custom  property
    onCollideEndCallback: null,     // custom  property
    onCollideActiveCallback: null,  // custom  property
    onCollideWith: {}               // custom  property*/


    private Body(Vector position, List<Vertex> vertices) {
        setDefaultValues(new InitValues("Body", vertices, position)); // Inicializo el body con los valores por defecto
    }

    public Body(float x, float y, float width, float height) {
        Vector position = new Vector(x, y);
        List<Vertex> vertices = Vertices.fromPath("L 0 0 L " + width + " 0 L " + width + " " + height + " L 0 " + height, this);
        setDefaultValues(new InitValues("Rectangle Body", vertices, position)); // Inicializo el body con los valores por defecto
    }

    // Creo el body desde un JSON
    public Body(float x, float y, JSONObject shapes, String shapeName) {
        setDefaultValues(new InitValues("Body", this)); // Inicializo el body con los valores por defecto
        parseBody(x, y, shapes, shapeName);
    }


    /**
     * Parses a body element exported by PhysicsEditor.
     *
     * @function Physics..PhysicsEditorParser.parseBody
     * @since 3.10.0
     *
     * @param {number} x - The horizontal world location of the body.
     * @param {number} y - The vertical world location of the body.
     * @param {object} config - The body configuration and fixture (child body) definitions, as exported by PhysicsEditor.
     * @param {Types.Physics..BodyConfig} [options] - An optional Body configuration object that is used to set initial Body properties on creation.
     *
     * @return {BodyType} A compound  JS Body.
     */
    private void parseBody(float x, float y, JSONObject shapes, String shapeName) {
        try {
            JSONObject bodyShape = shapes.getJSONObject(shapeName);

            JSONArray fixtureConfigs = bodyShape.getJSONArray("fixtures"); // Línea 41 src/physics/-js/PhysicsEditorParser.js
            List<Body> fixtures = new ArrayList<>();

            for (int fc = 0; fc < fixtureConfigs.length(); fc++) {
                List<Body> fixtureParts = parseFixture(fixtureConfigs.getJSONObject(fc));

                for (int i = 0; i < fixtureParts.size(); i++) {
                    fixtures.add(fixtureParts.get(i));
                }
            }

            this.label = bodyShape.getString("label");
            this.isStatic = bodyShape.getBoolean("isStatic");
            this.collisionFilter = new CollisionFilter(bodyShape.getJSONObject("collisionFilter"));

            setParts(fixtures);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Parses an element of the "fixtures" list exported by PhysicsEditor
     *
     * @function Physics..PhysicsEditorParser.parseFixture
     * @since 3.10.0
     *
     * @param {object} fixtureConfig - The fixture object to parse.
     *
     * @return {BodyType[]} - An array of  JS Bodies.
     */
    List<Body> parseFixture(JSONObject fixtureConfig) throws JSONException {
        List<Body> fixtures = null;

        if (fixtureConfig.has("circle")) {
            float x = (float) fixtureConfig.getDouble("x");
            float y = (float) fixtureConfig.getDouble("y");
            float r = (float) fixtureConfig.getDouble("radius");

        } else if (fixtureConfig.has("vertices")) {
            fixtures = this.parseVertices(fixtureConfig.getJSONArray("vertices"));
        }

        return fixtures;
    }

    /**
     * Parses the "vertices" lists exported by PhysicsEditor.
     *
     * @function Physics..PhysicsEditorParser.parseVertices
     * @since 3.10.0
     *
     * @param {array} vertexSets - The vertex lists to parse.
     * @param {Types.Physics..BodyConfig} [options] - An optional Body configuration object that is used to set initial Body properties on creation.
     *
     * @return {BodyType[]} - An array of  JS Bodies.
     */
    private List<Body> parseVertices(JSONArray vertexSets) throws JSONException {
        List<Body> parts = new ArrayList<>();

        for (int v = 0; v < vertexSets.length(); v++) {
            List<Vertex> vertexSet = parseVertexSet(vertexSets.getJSONArray(v));

            Collections.sort(vertexSet, new ClockwiseSort(vertexSet));

            Body part = new Body(Vertices.centre(vertexSet), vertexSet);

            parts.add(part);
        }

        // flag coincident part edges
        return Bodies.flagCoincidentParts(parts);
    }

    private List<Vertex> parseVertexSet(JSONArray vertexSet) throws JSONException {
        List<Vertex> vertices = new ArrayList<>();
        for (int v = 0; v < vertexSet.length(); v++) {
            float vx = (float) vertexSet.getJSONObject(v).getDouble("x");
            float vy = (float) vertexSet.getJSONObject(v).getDouble("y");

            vertices.add(new Vertex(vx, vy));
        }

        return vertices;
    }

    private void setDefaultValues(InitValues iv) {
        // Rectangle or Circle Shape. Ver SetBody en s.js
        this.id = idGen.incrementAndGet();
        this.type = "body";
        this.label = iv.label;
        this.parts = new ArrayList<>();
        this.angle = 0f;
        this.vertices = iv.vertices; //Vertices.fromPath("L 0 0 L 40 0 L 40 40 L 0 40", this);
        this.position = iv.position; //new Vector(0, 0);
        this.force = new Vector(0f, 0f);
        this.torque = 0f;
        this.positionImpulse = new Vector(0f, 0f);
        this.previousPositionImpulse = new Vector(0f, 0f);
        this.constraintImpulse = new Vector(0f, 0f);
        this.totalContacts = 0;
        this.speed = 0f;
        this.angularSpeed = 0f;
        this.velocity = new Vector(0f, 0f);
        this.angularVelocity = 0f;
        this.isSensor = false;
        this.isStatic = false;
        this.isSleeping = false;
        this.sleepCounter = 0f;
        this.motion = 0f;
        this.sleepThreshold = 60f;
        this.density = 0.001f;
        this.restitution = 0f;
        this.friction = 0.1f;
        this.frictionStatic = 0.5f;
        this.frictionAir = 0.01f;
        this.collisionFilter = new CollisionFilter(0, 0x0001, 0xFFFFFFFF);
        this.slop = 0.05f;
        this.timeScale = 1f;
        //events: null,
        this.bounds = null;
        //this.chamfer = null;
        this.circleRadius = 0f;
        this.positionPrev = null;
        this.anglePrev = 0f;
        this.parent = null;
        this.axes = null;
        this.area = 0f;
        this.mass = 0f;
        this.inverseMass = 0f;
        this.inertia = 0f;
        this.inverseInertia = 0f;
        this.original = null;
        this.gameObject = null;
        this.scale = new Vector(1f,1f);
        this.centerOfMass = new Vector(0f,0f);
        this.centerOffset = new Vector(0f,0f);
        this.gravityScale = new Vector(1f,1f);
        this.ignoreGravity = false;
        this.ignorePointer = false;
        this.region = null;

        initProperties();

        /*
        //  Helper function
        body.setOnCollideWith = function (body, callback)
        {
            if (callback)
            {
                this.onCollideWith[body.id] = callback;
            }
            else
            {
                delete this.onCollideWith[body.id];
            }

            return this;
        }
         */
    }

    /**
     * Initialises body properties.
     * @method _initProperties
     * @private
     * @param {body} body
     * @param {} [options]
     */
    private void initProperties() {
        // init required properties (order is important)
        if (this.bounds == null) this.bounds = new Bounds(vertices);
        if (this.positionPrev == null) this.positionPrev = new Vector(this.position);
        if (this.anglePrev == 0f) this.anglePrev = this.angle;
        setVertices(this.vertices); // Aquí cargo los Axes

        setParts(this.parts);
        setStatic(this.isStatic);
        setSleeping(this.isSleeping);
        if (this.parent == null) this.parent = this;

        Bounds bounds = this.bounds;

        Vertices.rotate(this.vertices, this.angle, this.position);
        Axes.rotate(this.axes, this.angle);
        bounds.update(this.vertices, this.velocity);

        // allow options to override the automatically calculated properties
        setMass(this.mass);
        setInertia(this.inertia);

        if (this.parts.size() == 1) {
            Vector centerOfMass = this.centerOfMass;
            Vector centerOffset = this.centerOffset;

            float bodyWidth = bounds.max.x - bounds.min.x;
            float bodyHeight = bounds.max.y - bounds.min.y;

            centerOfMass.x = -(bounds.min.x - this.position.x) / bodyWidth;
            centerOfMass.y = -(bounds.min.y - this.position.y) / bodyHeight;

            centerOffset.x = bodyWidth * centerOfMass.x;
            centerOffset.y = bodyHeight * centerOfMass.y;
        }

    }

    /**
     * Sets the body as static, including isStatic flag and setting mass and inertia to Infinity.
     * @method setStatic
     * @param {body} body
     * @param {bool} isStatic
     */
    public void setStatic(boolean isStatic) {
        for (int i = 0; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);

            part.isStatic = isStatic;

            if (isStatic) {
                part.original = new RigidBody();
                part.original.restitution = part.restitution;
                part.original.friction = part.friction;
                part.original.mass = part.mass;
                part.original.inertia = part.inertia;
                part.original.density = part.density;
                part.original.inverseMass = part.inverseMass;
                part.original.inverseInertia = part.inverseInertia;

                part.restitution = 0f;
                part.friction = 1f;
                part.mass = part.inertia = part.density = Float.POSITIVE_INFINITY;
                part.inverseMass = part.inverseInertia = 0f;

                part.positionPrev.x = part.position.x;
                part.positionPrev.y = part.position.y;
                part.anglePrev = part.angle;
                part.angularVelocity = 0f;
                part.speed = 0f;
                part.angularSpeed = 0f;
                part.motion = 0f;
            } else if (part.original != null) {
                part.restitution = part.original.restitution;
                part.friction = part.original.friction;
                part.mass = part.original.mass;
                part.inertia = part.original.inertia;
                part.density = part.original.density;
                part.inverseMass = part.original.inverseMass;
                part.inverseInertia = part.original.inverseInertia;

                part.original = null;
            }
        }
    }

    /**
     * Set a body as sleeping or awake.
     * @method set
     * @param {body} body
     * @param {boolean} isSleeping
     */
    void setSleeping(boolean isSleeping) {
        boolean wasSleeping = this.isSleeping;

        if (isSleeping) {
            this.isSleeping = true;
            this.sleepCounter = this.sleepThreshold;

            this.positionImpulse.x = 0f;
            this.positionImpulse.y = 0f;

            this.positionPrev.x = this.position.x;
            this.positionPrev.y = this.position.y;

            this.anglePrev = this.angle;
            this.speed = 0f;
            this.angularSpeed = 0f;
            this.motion = 0f;

            /*if (!wasSleeping) {
                Events.trigger(body, 'sleepStart');
            }*/
        } else {
            this.isSleeping = false;
            this.sleepCounter = 0f;

            /*if (wasSleeping) {
                Events.trigger(body, 'sleepEnd');
            }*/
        }
    }

    /**
     * Sets the mass of the body. Inverse mass, density and inertia are automatically updated to reflect the change.
     * @method setMass
     * @param {body} body
     * @param {number} mass
     */
    public void setMass(float mass) {
        float moment = this.inertia / (this.mass / 6f);
        this.inertia = moment * (mass / 6f);
        this.inverseInertia = 1f / this.inertia;

        this.mass = mass;
        this.inverseMass = 1f / this.mass;
        this.density = this.mass / this.area;
    }

    /**
     * Sets the density of the body. Mass and inertia are automatically updated to reflect the change.
     * @method setDensity
     * @param {body} body
     * @param {number} density
     */
    public void setDensity(float density) {
        this.setMass(density * this.area);
        this.density = density;
    }

    /**
     * Sets the moment of inertia (i.e. second moment of area) of the body.
     * Inverse inertia is automatically updated to reflect the change. Mass is not changed.
     * @method setInertia
     * @param {body} body
     * @param {number} inertia
     */
    public void setInertia(float inertia) {
        this.inertia = inertia;
        this.inverseInertia = 1f / this.inertia;
    };

    /**
     * Sets the body's vertices and updates body properties accordingly, including inertia, area and mass (with respect to `body.density`).
     * Vertices will be automatically transformed to be orientated around their centre of mass as the origin.
     * They are then automatically translated to world space based on `body.position`
     *
     * The `vertices` argument should be passed as an array of `Vector` points (or a `Vertices` array).
     * Vertices must form a convex hull, concave hulls are not supported.
     *
     * @method setVertices
     * @param {body} body
     * @param {vector[]} vertices
     */
    public void setVertices(List<Vertex> vertices) {
        // change vertices
        if (vertices.get(0).body == this) {
            this.vertices = vertices;
        } else {
            this.vertices = Vertices.fromVectors(vertices, this);
        }

        // update properties
        this.axes = Axes.fromVertices(this.vertices);
        this.area = Vertices.area(this.vertices);
        this.setMass(this.density * this.area);

        // orient vertices around the centre of mass at origin (0, 0)
        Vector centre = Vertices.centre(this.vertices);
        Vertices.translate(this.vertices, centre, -1f);

        // update inertia while vertices are at origin (0, 0)
        this.setInertia(Body.INERTIA_SCALE * Vertices.inertia(this.vertices, this.mass));

        // update geometry
        Vertices.translate(this.vertices, this.position);

        this.bounds.update(this.vertices, this.velocity);
    }

    /**
     * Sets the parts of the `body` and updates mass, inertia and centroid.
     * Each part will have its parent set to `body`
     * By default the convex hull will be automatically computed and set on `body`, unless `autoHull` is set to `false.`
     * Note that this method will ensure that the first part in `body.parts` will always be the `body`
     * @method setParts
     * @param {body} body
     * @param {body} parts
     * @param {bool} [autoHull=true]
     */
    public void setParts(List<Body> parts) {
        setParts(parts,true);
    }

    public void setParts(List<Body> parts, boolean autoHull) {
        /*if (this.parts == null) {
            this.parts = new ArrayList<>();
            this.parts.add(this);
        }*/

        // add all the parts, ensuring that the first part is always the parent body
        List<Body> cpyParts = new ArrayList<>(parts);

        this.parts.clear();
        this.parts.add(this);
        this.parent = this;

        //for (Body part : cpyParts) {
        for (int i = 0; i < cpyParts.size(); i++) {
            Body part = cpyParts.get(i);

            if (part != this) {
                part.parent = this;
                this.parts.add(part);
            }
        }

        if (this.parts.size() == 1)
            return;

        //autoHull = typeof autoHull !== 'undefined' ? autoHull : true;

        // find the convex hull of all parts to set on the parent body
        if (autoHull) {
            List<Vertex> vertices = new ArrayList<>();
//            for (Body part : cpyParts) {
            for (int i = 0; i < cpyParts.size(); i++) {
                Body part = cpyParts.get(i);
                vertices.addAll(part.vertices);
            }

            Collections.sort(vertices, new ClockwiseSort(vertices));

            List<Vertex> hull = Vertices.hull(vertices);
            Vector hullCentre = Vertices.centre(hull);

            this.setVertices(hull);
            Vertices.translate(this.vertices, hullCentre);
        }

        // sum the properties of all compound parts of the parent body
        RigidBody total = this.totalProperties();

        //   addition
        float cx = total.centre.x;
        float cy = total.centre.y;

        Bounds bounds = this.bounds;
        Vector centerOfMass = this.centerOfMass;
        Vector centerOffset = this.centerOffset;

        bounds.update(this.vertices, this.velocity);

        centerOfMass.x = -(bounds.min.x - cx) / (bounds.max.x - bounds.min.x);
        centerOfMass.y = -(bounds.min.y - cy) / (bounds.max.y - bounds.min.y);

        centerOffset.x = cx;
        centerOffset.y = cy;

        this.area = total.area;
        this.parent = this;
        this.position.x = cx;
        this.position.y = cy;

        this.positionPrev.x = cx;
        this.positionPrev.y = cy;

        this.setMass(total.mass);
        this.setInertia(total.inertia);
    }

    /**
     * Set the centre of mass of the body.
     * The `centre` is a vector in world-space unless `relative` is set, in which case it is a translation.
     * The centre of mass is the point the body rotates about and can be used to simulate non-uniform density.
     * This is equal to moving `body.position` but not the `body.vertices`
     * Invalid if the `centre` falls outside the body's convex hull.
     * @method setCentre
     * @param {body} body
     * @param {vector} centre
     * @param {bool} relative
     */
    public void setCentre(Vector centre, boolean relative) {
        if (!relative) {
            this.positionPrev.x = centre.x - (this.position.x - this.positionPrev.x);
            this.positionPrev.y = centre.y - (this.position.y - this.positionPrev.y);
            this.position.x = centre.x;
            this.position.y = centre.y;
        } else {
            this.positionPrev.x += centre.x;
            this.positionPrev.y += centre.y;
            this.position.x += centre.x;
            this.position.y += centre.y;
        }
    }

    /**
     * Sets the position of the body instantly. Velocity, angle, force etc. are unchanged.
     * @method setPosition
     * @param {body} body
     * @param {vector} position
     */
    public void setPosition(Vector position) {
        Vector delta = Vector.sub(position, this.position, null);

        this.positionPrev.x += delta.x;
        this.positionPrev.y += delta.y;

        //for (Body part : this.parts) {
        for (int i = 0; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);

            part.position.x += delta.x;
            part.position.y += delta.y;
            Vertices.translate(part.vertices, delta);

            part.bounds.update(part.vertices, this.velocity);
        }

    }

    /**
     * Sets the angle of the body instantly. Angular velocity, position, force etc. are unchanged.
     * @method setAngle
     * @param {body} body
     * @param {number} angle
     */
    public void setAngle(float angle) {
        float delta = angle - this.angle;
        this.anglePrev += delta;

        for (int i = 0; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);

            part.angle += delta;
            Vertices.rotate(part.vertices, delta, this.position);
            Axes.rotate(part.axes, delta);
            part.bounds.update(part.vertices, this.velocity);
            if (i > 0) {
                Vector.rotateAbout(part.position, delta, this.position, part.position);
            }
        }
    }

    /**
     * Sets the linear velocity of the body instantly. Position, angle, force etc. are unchanged. See also `Body.applyForce`
     * @method setVelocity
     * @param {body} body
     * @param {vector} velocity
     */
    public void setVelocity(Vector velocity) {
        this.positionPrev.x = this.position.x - velocity.x;
        this.positionPrev.y = this.position.y - velocity.y;
        this.velocity.x = velocity.x;
        this.velocity.y = velocity.y;
        this.speed = Vector.magnitude(this.velocity);
    }

    /**
     * Sets the angular velocity of the body instantly. Position, angle, force etc. are unchanged. See also `Body.applyForce`
     * @method setAngularVelocity
     * @param {body} body
     * @param {number} velocity
     */
    public void setAngularVelocity(float velocity) {
        this.anglePrev = this.angle - velocity;
        this.angularVelocity = velocity;
        this.angularSpeed = Math.abs(this.angularVelocity);
    }

    /**
     * Moves a body by a given vector relative to its current position, without imparting any velocity.
     * @method translate
     * @param {body} body
     * @param {vector} translation
     */
    public void translate(Vector translation) {
        this.setPosition(Vector.add(this.position, translation, null));
    }

    /**
     * Rotates a body by a given angle relative to its current angle, without imparting any angular velocity.
     * @method rotate
     * @param {body} body
     * @param {number} rotation
     * @param {vector} [point]
     */
    public void rotate(float rotation, Vector point) {
        if (point == null) {
            this.setAngle(this.angle + rotation);
        } else {
            float cos = (float) Math.cos(rotation);
            float sin = (float) Math.sin(rotation);
            float dx = this.position.x - point.x;
            float dy = this.position.y - point.y;

            this.setPosition(new Vector(point.x + (dx * cos - dy * sin),point.y + (dx * sin + dy * cos)));
            this.setAngle(this.angle + rotation);
        }
    }

    /**
     * Scales the body, including updating physical properties (mass, area, axes, inertia), from a world-space point (default is body centre).
     * @method scale
     * @param {body} body
     * @param {number} scaleX
     * @param {number} scaleY
     * @param {vector} [point]
     */
    public void scale(float scaleX, float scaleY, Vector point) {
        float totalArea = 0f, totalInertia = 0f;

        if (point == null) {
            point = this.position;
        }

        //Log.i("FIRING-TEST", "Body Parts: " + this.parts.size());
        for (int i = 0; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);

            part.scale.x = scaleX;
            part.scale.y = scaleY;

            // scale vertices
            Vertices.scale(part.vertices, scaleX, scaleY, point);

            // update properties
            part.axes = Axes.fromVertices(part.vertices);
            part.area = Vertices.area(part.vertices);
            part.setMass(this.density * part.area);

            // update inertia (requires vertices to be at origin)
            Vertices.translate(part.vertices, new Vector(-part.position.x, -part.position.y));
            part.setInertia(Body.INERTIA_SCALE * Vertices.inertia(part.vertices, part.mass));
            Vertices.translate(part.vertices, new Vector(part.position.x, part.position.y));

            if (i > 0) {
                totalArea += part.area;
                totalInertia += part.inertia;
            }

            // scale position
            part.position.x = point.x + (part.position.x - point.x) * scaleX;
            part.position.y = point.y + (part.position.y - point.y) * scaleY;

            // update bounds
            part.bounds.update(part.vertices, this.velocity);
        }

        // handle parent body
        if (this.parts.size() > 1) {
            this.area = totalArea;

            if (!this.isStatic) {
                this.setMass(this.density * totalArea);
                this.setInertia(totalInertia);
            }
        }

        // handle circles
        if (this.circleRadius > 0f) {
            if (scaleX == scaleY) {
                this.circleRadius *= scaleX;
            } else {
                // body is no longer a circle
                this.circleRadius = 0f;
            }
        }
    }

    /**
     * Performs a simulation step for the given `body`, including updating position and angle using Verlet integration.
     * @method update
     * @param {body} body
     * @param {number} deltaTime
     * @param {number} timeScale
     * @param {number} correction
     */
    public void update(float deltaTime) {
        float deltaTimeSquared = (float) Math.pow(deltaTime * this.timeScale, 2);

        // from the previous step
        float frictionAir = 1f - this.frictionAir * this.timeScale;
        float velocityPrevX = this.position.x - this.positionPrev.x;
        float velocityPrevY = this.position.y - this.positionPrev.y;

        // update velocity with Verlet integration
        this.velocity.x = (velocityPrevX * frictionAir) + (this.force.x / this.mass) * deltaTimeSquared;
        this.velocity.y = (velocityPrevY * frictionAir) + (this.force.y / this.mass) * deltaTimeSquared;

        this.positionPrev.x = this.position.x;
        this.positionPrev.y = this.position.y;
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;

        // update angular velocity with Verlet integration
        this.angularVelocity = ((this.angle - this.anglePrev) * frictionAir) + (this.torque / this.inertia) * deltaTimeSquared;
        this.anglePrev = this.angle;
        this.angle += this.angularVelocity;

        // track speed and acceleration
        this.speed = Vector.magnitude(this.velocity);
        this.angularSpeed = Math.abs(this.angularVelocity);

        // transform the body geometry
        for (int i = 0; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);

            Vertices.translate(part.vertices, this.velocity);

            if (i > 0) {
                part.position.x += this.velocity.x;
                part.position.y += this.velocity.y;
            }

            if (this.angularVelocity != 0) {
                Vertices.rotate(part.vertices, this.angularVelocity, this.position);
                Axes.rotate(part.axes, this.angularVelocity);
                if (i > 0) {
                    Vector.rotateAbout(part.position, this.angularVelocity, this.position, part.position);
                }
            }

            part.bounds.update(part.vertices, this.velocity);
        }
    }

    /**
     * Applies a force to a body from a given world-space position, including resulting torque.
     * @method applyForce
     * @param {body} body
     * @param {vector} position
     * @param {vector} force
     */
    public void applyForce(Vector position, Vector force) {
        this.force.x += force.x;
        this.force.y += force.y;
        Vector offset = new Vector(position.x - this.position.x, position.y - this.position.y);
        this.torque += offset.x * force.y - offset.y * force.x;
    }

    /**
     * Returns the sums of the properties of all compound parts of the parent body.
     * @method totalProperties
     * @private
     * @param {body} body
     * @return {}
     */
    private RigidBody totalProperties() {
        // from equations at:
        // https://ecourses.ou.edu/cgi-bin/ebook.cgi?doc=&topic=st&chap_sec=07.2&page=theory
        // http://output.to/sideway/default.asp?qno=121100087

        RigidBody properties = new RigidBody();

        // sum the properties of all compound parts of the parent body
        for (int i = (this.parts.size() == 1) ? 0 : 1; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);
            float mass = (part.mass != Float.POSITIVE_INFINITY) ? part.mass : 1f;

            properties.mass += mass;
            properties.area += part.area;
            properties.inertia += part.inertia;
            properties.centre = Vector.add(properties.centre, Vector.mult(part.position, mass), null);
        }

        properties.centre = Vector.div(properties.centre, properties.mass);

        return properties;
    }

    /**
     * Sets the collision category of this Game Object's  Body. This number must be a power of two between 2^0 (= 1) and 2^31.
     * Two bodies with different collision groups (see {@link #setCollisionGroup}) will only collide if their collision
     * categories are included in their collision masks (see {@link #setCollidesWith}).
     *
     * @method Physics..Components.Collision#setCollisionCategory
     * @since 3.0.0
     *
     * @param {number} value - Unique category bitfield.
     *
     * @return {GameObjects.GameObject} This Game Object.
     * Muy buena explicación: http://www.iforce2d.net/b2dtut/collision-filtering
     */
    public void setCollisionCategory(int value) {
        this.collisionFilter.category = value;
    }

    /**
     * Sets the collision group of this Game Object's  Body. If this is zero or two  Bodies have different values,
     * they will collide according to the usual rules (see {@link #setCollisionCategory} and {@link #setCollisionGroup}).
     * If two  Bodies have the same positive value, they will always collide; if they have the same negative value,
     * they will never collide.
     *
     * @method Physics..Components.Collision#setCollisionGroup
     * @since 3.0.0
     *
     * @param {number} value - Unique group index.
     *
     * @return {GameObjects.GameObject} This Game Object.
     * Muy buena explicación: http://www.iforce2d.net/b2dtut/collision-filtering
     */
    public void setCollisionGroup(int value) {
        this.collisionFilter.group = value;
    }

    /**
     * Sets the collision mask for this Game Object's  Body. Two  Bodies with different collision groups will only
     * collide if each one includes the other's category in its mask based on a bitwise AND, i.e. `(categoryA & maskB) !== 0`
     * and `(categoryB & maskA) !== 0` are both true.
     *
     * @method Physics..Components.Collision#setCollidesWith
     * @since 3.0.0
     *
     * @param {(number|number[])} categories - A unique category bitfield, or an array of them.
     *
     * @return {GameObjects.GameObject} This Game Object.
     */
    public void setCollidesWith(int categories) {
        this.collisionFilter.mask = categories;
    }

    /*public Object clone() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch(CloneNotSupportedException ex){
            System.out.println(" no se puede duplicar");
        }
        return obj;
    }*/


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Internal Classes
    ////////////////////////////////////////////////////////////////////////////////////////////////
    class RigidBody {
        float restitution;
        float friction;
        float mass;
        float area;
        float inertia;
        float density;
        float inverseMass;
        float inverseInertia;
        Vector centre;
        
        RigidBody() {
            this.restitution = 0f;
            this.friction = 0f;
            this.mass = 0f;
            this.area = 0f;
            this.inertia = 0f;
            this.inertia = 0f;
            this.density = 0f;
            this.inverseMass = 0f;
            this.inverseInertia = 0f;
            this.centre = new Vector();    
        }

        @NonNull
        @Override
        public String toString() {
            return "mass: " + mass + ", area: " + area + " inertia: " + inertia + " centre: " + centre.toString();
        }
    }

    class InitValues {
        public String label;
        public Vector position;
        public List<Vertex> vertices;

        InitValues(String label, Body body) {
            this.label = label;
            this.vertices = Vertices.fromPath("L 0 0 L 40 0 L 40 40 L 0 40", body);
            this.position = new Vector(0f, 0f);
        }

        InitValues(String label, List<Vertex> vertices, Vector position) {
            this.label = label;
            this.vertices = vertices;
            this.position = position;
        }

        /*InitValues(String label, List<Vertex> vertices, Vector position) {
            this.label = label;
            this.vertices = vertices;
            this.position = position;
        }*/

    }

    public void collisionStart(Pair data) {
        if (listener != null) listener.onCollideStart(data);

        if (simpleListener != null) {
            Body self, other;
            if (data.bodyA.id == this.id) {
                self = data.bodyA;
                other = data.bodyB;
            } else {
                self = data.bodyB;
                other = data.bodyA;
            }
            simpleListener.onCollide(data, self, other);
        }
    }

    public void collisionActive(Pair data) {
        if (listener != null) listener.onCollideActive(data);
    }

    public void collisionEnd(Pair data) {
        if (listener != null) listener.onCollideEnd(data);
    }

    public void setLabel(String label) {
        this.label = label;
        for (int i = 0; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);
            part.label = label;
        }
    }

    public void setGameObject(GameObject gameObject) {
        this.gameObject = gameObject;
        for (int i = 0; i < this.parts.size(); i++) {
            Body part = this.parts.get(i);
            part.gameObject = gameObject;
        }
    }


    public void setCollisionListener(BodyCollisionListener listener) {
        this.listener = listener;

        for (int i = 0; i < this.parts.size(); i++) {
            this.parts.get(i).listener = listener;
        }
    }

    public void setSimpleCollisionListener(BodySimpleCollisionListener listener) {
        this.simpleListener = listener;

        for (int i = 0; i < this.parts.size(); i++) {
            this.parts.get(i).simpleListener = listener;
        }
    }



}
