package test.core;

import sdk.geometry.Sphere;
import sdk.geometry.Vector3D;

public class GameObject3DTest {
    public final Vector3D position;
    public final Sphere bounds;
    public final Vector3D velocity;
    public final Vector3D accel;

    public GameObject3DTest(float x, float y, float z, float radius) {
        this.position = new Vector3D(x,y,z);
        this.bounds = new Sphere(x, y, z, radius);
        this.velocity = new Vector3D();
        this.accel = new Vector3D();
    }
}
