package test.collision;

import sdk.core.Body;
import sdk.geometry.Vector;
import sdk.geometry.Vertex;

import java.util.List;

/**
 * Contains methods for changing the collision filter of a  Body. Should be used as a mixin and not called directly.
 *
 * @namespace Physics..Components.Collision
 * @since 3.0.0
 */
public class CollisionTest {
    Body axisBody;
    float depth;
    public Body bodyA;
    public Body bodyB;
    public Body parentA;
    public Body parentB;
    List<Vertex> supports;
    Vector normal;
    Vector tangent;
    Vector penetration;
    boolean reused;
    boolean collided;
    int axisNumber;

    public Collision(Body bodyA, Body bodyB, boolean collided) {
        this.bodyA = bodyA;
        this.bodyB = bodyB;
        this.collided = collided;
        this.penetration = new Vector();
    }

    /**
     * Sets the collision mask for this Game Object's  Body. Two  Bodies with different collision groups will only
     * collide if each one includes the other's category in its mask based on a bitwise AND, i.e. `(categoryA & maskB) !== 0`
     * and `(categoryB & maskA) !== 0` are both true.
     *
     * @method Physics..Components.Collision#setCollidesWith
     * @since 3.0.0
     *
     * @param {(number|number[])} categories - A unique category bitfield, or an array of them.
     *
     * @return {GameObjects.GameObject} This Game Object.
     */
    /*setCollidesWith: function (categories)
    {
        var flags = 0;

        if (!Array.isArray(categories))
        {
            flags = categories;
        }
        else
        {
            for (var i = 0; i < categories.length; i++)
            {
                flags |= categories[i];
            }
        }

        this.body.collisionFilter.mask = flags;

        return this;
    }*/


}
