package test.collision;

import sdk.core.Body;
import sdk.geometry.Vector;
import sdk.geometry.Vertex;
import sdk.geometry.Vertices;

import java.util.ArrayList;
import java.util.List;

/**
 * The `SAT` feature contains methods for detecting collisions using the Separating Axis Theorem.
 *
 * @class SAT
 */
public class SATTest {

    /**
     * Detect collision between two bodies using the Separating Axis Theorem.
     * @method collides
     * @param {body} bodyA
     * @param {body} bodyB
     * @param {collision} previousCollision
     * @return {collision} collision
     */
    public static Collision collides(Body bodyA, Body bodyB, Collision previousCollision) {
        Overlap overlapAB;
        Overlap overlapBA;
        Overlap minOverlap;
        Collision collision;
        boolean canReusePrevCol = false;

        if (previousCollision != null) {
            // estimate total motion
            Body parentA = bodyA.parent;
            Body parentB = bodyB.parent;
            float motion = parentA.speed * parentA.speed + parentA.angularSpeed * parentA.angularSpeed + parentB.speed * parentB.speed + parentB.angularSpeed * parentB.angularSpeed;

            // we may be able to (partially) reuse collision result
            // but only safe if collision was resting
            canReusePrevCol = previousCollision.collided && motion < 0.2;

            // reuse collision object
            collision = previousCollision;
        } else {
            collision = new Collision(bodyA, bodyB, false);
        }

        if (previousCollision != null && canReusePrevCol) {
            // if we can reuse the collision result
            // we only need to test the previously found axis
            Body axisBodyA = collision.axisBody;
            Body axisBodyB = axisBodyA == bodyA ? bodyB : bodyA;
            List<Vector> axes = new ArrayList<>();
            axes.add(axisBodyA.axes.get(previousCollision.axisNumber));

            minOverlap = SAT.overlapAxes(axisBodyA.vertices, axisBodyB.vertices, axes);
            collision.reused = true;

            if (minOverlap.overlap <= 0) {
                collision.collided = false;
                return collision;
            }
        } else {
            // if we can't reuse a result, perform a full SAT test

            overlapAB = SAT.overlapAxes(bodyA.vertices, bodyB.vertices, bodyA.axes);

            if (overlapAB.overlap <= 0) {
                collision.collided = false;
                return collision;
            }

            overlapBA = SAT.overlapAxes(bodyB.vertices, bodyA.vertices, bodyB.axes);

            if (overlapBA.overlap <= 0) {
                collision.collided = false;
                return collision;
            }

            if (overlapAB.overlap < overlapBA.overlap) {
                minOverlap = overlapAB;
                collision.axisBody = bodyA;
            } else {
                minOverlap = overlapBA;
                collision.axisBody = bodyB;
            }

            // important for reuse later
            collision.axisNumber = minOverlap.axisNumber;
        }

        collision.bodyA = bodyA.id < bodyB.id ? bodyA : bodyB;
        collision.bodyB = bodyA.id < bodyB.id ? bodyB : bodyA;
        collision.collided = true;
        collision.depth = minOverlap.overlap;
        collision.parentA = collision.bodyA.parent;
        collision.parentB = collision.bodyB.parent;

        bodyA = collision.bodyA;
        bodyB = collision.bodyB;

        // ensure normal is facing away from bodyA
        if (Vector.dot(minOverlap.axis, Vector.sub(bodyB.position, bodyA.position, null)) < 0) {
            collision.normal = new Vector(minOverlap.axis.x, minOverlap.axis.y);
        } else {
            collision.normal = new Vector(-minOverlap.axis.x, -minOverlap.axis.y);
        }

        collision.tangent = Vector.perp(collision.normal, false);

        //collision.penetration = collision.penetration || {};
        collision.penetration.x = collision.normal.x * collision.depth;
        collision.penetration.y = collision.normal.y * collision.depth;

        // find support points, there is always either exactly one or two
        List<Vertex> verticesB = SAT.findSupports(bodyA, bodyB, collision.normal);
        List<Vertex> supports = new ArrayList<>();

        // find the supports from bodyB that are inside bodyA
        if (Vertices.contains(bodyA.vertices, verticesB.get(0))) {
            supports.add(verticesB.get(0));
        }

        if (Vertices.contains(bodyA.vertices, verticesB.get(1))) {
            supports.add(verticesB.get(1));
        }

        // find the supports from bodyA that are inside bodyB
        if (supports.size() < 2) {
            List<Vertex> verticesA = SAT.findSupports(bodyB, bodyA, Vector.neg(collision.normal));

            if (Vertices.contains(bodyB.vertices, verticesA.get(0))) {
                supports.add(verticesA.get(0));
            }

            if (supports.size() < 2 && Vertices.contains(bodyB.vertices, verticesA.get(1))) {
                supports.add(verticesA.get(1));
            }
        }

        // account for the edge case of overlapping but no vertex containment
        if (supports.size() < 1) {
            supports.add(verticesB.get(0));
        }

        collision.supports = supports;

        return collision;
    }

    /**
     * Find the overlap between two sets of vertices.
     * @method _overlapAxes
     * @private
     * @param {} verticesA
     * @param {} verticesB
     * @param {} axes
     * @return result
     */
    private static Overlap overlapAxes(List<Vertex> verticesA, List<Vertex> verticesB, List<Vector> axes) {
        Projection projectionA = new Projection(); // Vector._temp[0]
        Projection projectionB = new Projection(); // Vector._temp[1]
        Overlap result = new Overlap();
        float overlap;
        Vector axis;

        for (int i = 0; i < axes.size(); i++) {
            axis = axes.get(i);

            SAT.projectToAxis(projectionA, verticesA, axis);
            SAT.projectToAxis(projectionB, verticesB, axis);

            overlap = Math.min(projectionA.max - projectionB.min, projectionB.max - projectionA.min);

            if (overlap <= 0) {
                result.overlap = overlap;
                return result;
            }

            if (overlap < result.overlap) {
                result.overlap = overlap;
                result.axis = axis;
                result.axisNumber = i;
            }
        }

        return result;
    }

    /**
     * Projects vertices on an axis and returns an interval.
     * @method projectToAxis
     * @private
     * @param {} projection
     * @param {} vertices
     * @param {} axis
     */
    private static void projectToAxis(Projection projection, List<Vertex> vertices, Vector axis) {
        float min = Vector.dot(vertices.get(0), axis);
        float max = min;

        for (int i = 1; i < vertices.size(); i+=1) {
            float dot = Vector.dot(vertices.get(i), axis);

            if (dot > max) {
                max = dot;
            } else if (dot < min) {
                min = dot;
            }
        }

        projection.min = min;
        projection.max = max;
    }

    /**
     * Finds supporting vertices given two bodies along a given direction using hill-climbing.
     * @method _findSupports
     * @private
     * @param {} bodyA
     * @param {} bodyB
     * @param {} normal
     * @return [vector]
     */
    public static List<Vertex> findSupports(Body bodyA, Body bodyB, Vector normal) {
        float nearestDistance = Integer.MAX_VALUE;
        Vector vertexToBody = new Vector();
        List<Vertex> vertices = bodyB.vertices;
        Vector bodyAPosition = bodyA.position;
        float distance;
        Vertex vertex, vertexA = null, vertexB;

        // find closest vertex on bodyB
        for (int i = 0; i < vertices.size(); i++) {
            vertex = vertices.get(i);
            vertexToBody.x = vertex.x - bodyAPosition.x;
            vertexToBody.y = vertex.y - bodyAPosition.y;
            distance = -Vector.dot(normal, vertexToBody);

            if (distance < nearestDistance) {
                nearestDistance = distance;
                vertexA = vertex;
            }
        }

        // find next closest vertex using the two connected to it
        int prevIndex = (vertexA.index - 1 >= 0) ? vertexA.index - 1 : vertices.size() - 1;
        vertex = vertices.get(prevIndex);
        vertexToBody.x = vertex.x - bodyAPosition.x;
        vertexToBody.y = vertex.y - bodyAPosition.y;
        nearestDistance = -Vector.dot(normal, vertexToBody);
        vertexB = vertex;

        int nextIndex = (vertexA.index + 1) % vertices.size();
        vertex = vertices.get(nextIndex);
        vertexToBody.x = vertex.x - bodyAPosition.x;
        vertexToBody.y = vertex.y - bodyAPosition.y;
        distance = -Vector.dot(normal, vertexToBody);
        if (distance < nearestDistance) {
            vertexB = vertex;
        }

        // Result
        List<Vertex> result = new ArrayList<>();
        result.add(vertexA);
        result.add(vertexB);
        return result;
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Internal Classes
    ////////////////////////////////////////////////////////////////////////////////////////////////
    static class Overlap {
        int axisNumber;
        float overlap;
        public Vector axis;

        Overlap() {
            overlap = Integer.MAX_VALUE;
        }

    }

    static class Projection {
        float min, max;

        Projection() {
        }

    }

}
