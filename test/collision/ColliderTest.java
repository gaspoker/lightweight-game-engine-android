package test.collision;

import sdk.core.Body;
import sdk.core.Scene;
import sdk.geometry.Bounds;
import sdk.geometry.Vertex;
import sdk.opengl.GLGame;
import sdk.opengl.GLGraphics;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ColliderTest {
    public interface CollisionListener {
        void onCollisionStart(List<Pair> pairs);
        void onCollisionActive(List<Pair> pairs);
        void onCollisionEnd(List<Pair> pairs);
    }

    private static int nextCategory = 0x0001;

    public final Pairs pairs;
    //public final List<Pair> broadphasePairs;
    public final List<Body> bodies;
    Grid broadphase;
    CollisionListener listener;
    public Bounds bounds;
    private Scene scene;
    private GLGraphics graphics;

    // DEBUG VARIABLES
    // DEBUG VARIABLES
    // DEBUG VARIABLES
    private boolean debug = false;
    // DEBUG VARIABLES
    // DEBUG VARIABLES
    // DEBUG VARIABLES

    /**
     * Returns the next unique category bitfield (starting after the initial default category `0x0001`).
     * There are 32 available. See .js `body.collisionFilter` for more information.
     * @method nextCategory
     * @return {Number} Unique category bitfield
     */
    public static int nextCategory() {
        nextCategory = nextCategory << 1;
        return nextCategory;
    }


    public Collider(Scene scene, CollisionListener listener) {
        this.debug = false;

        this.scene = scene;
        this.listener = listener;
        this.graphics = ((GLGame)scene.game).getGLGraphics();

        this.pairs = new Pairs();
        this.bodies = new ArrayList<>();

        this.bounds = new Bounds(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY); // según World.js de -js

        // Inicialización colisiones
        this.broadphase = new Grid(this);
        //broadphasePairs = new ArrayList<>();
    }

    public void addBody(Body body) {
        this.bodies.add(body);
    }

    public void removeBody(Body body) {
        this.bodies.remove(body);
    }

    public List<Body> getBodies() {
        return this.bodies;
    }

    public void update(long timestamp, float deltaTime) {


        // update all body position and rotation by integration
        bodiesUpdate(deltaTime);

        // if world is dirty, we must flush the whole grid
//        if (world.isModified)
//            broadphase.controller.clear(broadphase);

        // update the grid buckets based on current bodies
        broadphase.update(bodies, false);

        // narrowphase pass: find actual collisions, then create or update collision pairs
        List<Collision> collisions = broadphase.detector.collisions(broadphase.pairsList);

        // update collision pairs
        pairs.update(collisions, timestamp);
        pairs.removeOld(timestamp);

        // trigger collision events
        if (pairs.collisionStart.size() > 0) {
            collisionStart(pairs.collisionStart);
            //Events.trigger(engine, 'collisionStart', { pairs: pairs.collisionStart });
        }

        // trigger collision events
        if (pairs.collisionActive.size() > 0) {
            collisionActive(pairs.collisionActive);
            //Events.trigger(engine, 'collisionActive', { pairs: pairs.collisionActive });
        }

        if (pairs.collisionEnd.size() > 0) {
            collisionEnd(pairs.collisionEnd);
            //Events.trigger(engine, 'collisionEnd', { pairs: pairs.collisionEnd });
        }

    } // update


    public void collisionStart(List<Pair> pairs) {
        if (listener != null) {
            listener.onCollisionStart(pairs);
        }

        for (int i = 0; i < pairs.size(); i++) {
            Pair pair = pairs.get(i);
            Body bodyA = pair.bodyA;
            Body bodyB = pair.bodyB;

            bodyA.collisionStart(pair);
            bodyB.collisionStart(pair);
        }
    }


    public void collisionActive(List<Pair> pairs) {
        if (listener != null) {
            listener.onCollisionActive(pairs);
        }

        for (int i=0; i<pairs.size(); i++) {
            Pair pair = pairs.get(i);
            Body bodyA = pair.bodyA;
            Body bodyB = pair.bodyB;

            bodyA.collisionActive(pair);
            bodyB.collisionActive(pair);
        }
    }


    public void collisionEnd(List<Pair> pairs) {
        if (listener != null) {
            listener.onCollisionEnd(pairs);
        }

        for (int i=0; i<pairs.size(); i++) {
            Pair pair = pairs.get(i);
            Body bodyA = pair.bodyA;
            Body bodyB = pair.bodyB;

            bodyA.collisionEnd(pair);
            bodyB.collisionEnd(pair);
        }
    }

    public void present(float deltaTime) {
        if (this.debug) {
            renderBodies(this.bodies, 0x106909, 1f);
            renderGrid(this.broadphase, 0xffb400, 0.5f);
        }
    }


    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * Renders the Engine Broadphase Controller Grid to the given Graphics instance.
     *
     * The debug renderer calls this method if the `showBroadphase` config value is set.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render the Grid to your own Graphics instance.
     *
     * @method Physics..World#renderGrid
     * @since 3.22.0
     *
     * @param {Grid} grid - The  Grid to be rendered.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {number} lineColor - The line color.
     * @param {number} lineOpacity - The line opacity, between 0 and 1.
     *
     * @return {this} This  World instance for method chaining.
     */
    private void renderGrid(Grid grid, int lineColor, float lineOpacity) {
        final String regex = "C([0-9]+)R([0-9]+)";
        final Pattern pattern = Pattern.compile(regex);
        Matcher matcher;

        Set<String> bucketKeys = grid.buckets.keySet();

        for (String bucketId : bucketKeys) {
            //Log.i("COLLIDER-TEST", bucketId);

            if (grid.buckets.get(bucketId).size() < 2) {
                continue;
            }

            matcher = pattern.matcher(bucketId);

            if (matcher.find()) {
                float col = Float.parseFloat(matcher.group(1));
                float row = Float.parseFloat(matcher.group(2));

                graphics.drawRect(col * grid.bucketWidth, row * grid.bucketHeight, grid.bucketWidth, grid.bucketHeight, lineColor, lineOpacity);
            }

        }

    }

    /**
     * Renders the list of Pair separations to the given Graphics instance.
     *
     * The debug renderer calls this method if the `showSeparations` config value is set.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render the Grid to your own Graphics instance.
     *
     * @method Physics..World#renderSeparations
     * @since 3.22.0
     *
     * @param {Pair[]} pairs - An array of  Pairs to be rendered.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {number} lineColor - The line color.
     *
     * @return {this} This  World instance for method chaining.
     */
    /*renderSeparations: function (pairs, graphics, lineColor)
    {
        graphics.lineStyle(1, lineColor, 1);

        for (var i = 0; i < pairs.length; i++)
        {
            var pair = pairs[i];

            if (!pair.isActive)
            {
                continue;
            }

            var collision = pair.collision;
            var bodyA = collision.bodyA;
            var bodyB = collision.bodyB;
            var posA = bodyA.position;
            var posB = bodyB.position;
            var penetration = collision.penetration;

            var k = (!bodyA.isStatic && !bodyB.isStatic) ? 4 : 1;

            if (bodyB.isStatic)
            {
                k = 0;
            }

            graphics.lineBetween(
                    posB.x,
                    posB.y,
                    posB.x - (penetration.x * k),
                    posB.y - (penetration.y * k)
            );

            k = (!bodyA.isStatic && !bodyB.isStatic) ? 4 : 1;

            if (bodyA.isStatic)
            {
                k = 0;
            }

            graphics.lineBetween(
                    posA.x,
                    posA.y,
                    posA.x - (penetration.x * k),
                    posA.y - (penetration.y * k)
            );
        }

        return this;
    }*/

    /**
     * Renders the list of collision points and normals to the given Graphics instance.
     *
     * The debug renderer calls this method if the `showCollisions` config value is set.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render the Grid to your own Graphics instance.
     *
     * @method Physics..World#renderCollisions
     * @since 3.22.0
     *
     * @param {Pair[]} pairs - An array of  Pairs to be rendered.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {number} lineColor - The line color.
     *
     * @return {this} This  World instance for method chaining.
     */
    /*renderCollisions: function (pairs, graphics, lineColor)
    {
        graphics.lineStyle(1, lineColor, 0.5);
        graphics.fillStyle(lineColor, 1);

        var i;
        var pair;

        //  Collision Positions

        for (i = 0; i < pairs.length; i++)
        {
            pair = pairs[i];

            if (!pair.isActive)
            {
                continue;
            }

            for (var j = 0; j < pair.activeContacts.length; j++)
            {
                var contact = pair.activeContacts[j];
                var vertex = contact.vertex;

                graphics.fillRect(vertex.x - 2, vertex.y - 2, 5, 5);
            }
        }

        //  Collision Normals

        for (i = 0; i < pairs.length; i++)
        {
            pair = pairs[i];

            if (!pair.isActive)
            {
                continue;
            }

            var collision = pair.collision;
            var contacts = pair.activeContacts;

            if (contacts.length > 0)
            {
                var normalPosX = contacts[0].vertex.x;
                var normalPosY = contacts[0].vertex.y;

                if (contacts.length === 2)
                {
                    normalPosX = (contacts[0].vertex.x + contacts[1].vertex.x) / 2;
                    normalPosY = (contacts[0].vertex.y + contacts[1].vertex.y) / 2;
                }

                if (collision.bodyB === collision.supports[0].body || collision.bodyA.isStatic)
                {
                    graphics.lineBetween(
                            normalPosX - collision.normal.x * 8,
                            normalPosY - collision.normal.y * 8,
                            normalPosX,
                            normalPosY
                    );
                }
                else
                {
                    graphics.lineBetween(
                            normalPosX + collision.normal.x * 8,
                            normalPosY + collision.normal.y * 8,
                            normalPosX,
                            normalPosY
                    );
                }
            }
        }

        return this;
    }*/

    /**
     * Renders the bounds of an array of Bodies to the given Graphics instance.
     *
     * If the body is a compound body, it will render the bounds for the parent compound.
     *
     * The debug renderer calls this method if the `showBounds` config value is set.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render bounds to your own Graphics instance.
     *
     * @method Physics..World#renderBodyBounds
     * @since 3.22.0
     *
     * @param {array} bodies - An array of bodies from the localWorld.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {number} lineColor - The line color.
     * @param {number} lineOpacity - The line opacity, between 0 and 1.
     */
    /*renderBodyBounds: function (bodies, graphics, lineColor, lineOpacity)
    {
        graphics.lineStyle(1, lineColor, lineOpacity);

        for (var i = 0; i < bodies.length; i++)
        {
            var body = bodies[i];

            //  1) Don't show invisible bodies
            if (!body.render.visible)
            {
                continue;
            }

            var bounds = body.bounds;

            if (bounds)
            {
                graphics.strokeRect(
                        bounds.min.x,
                        bounds.min.y,
                        bounds.max.x - bounds.min.x,
                        bounds.max.y - bounds.min.y
                );
            }
            else
            {
                var parts = body.parts;

                for (var j = parts.length > 1 ? 1 : 0; j < parts.length; j++)
                {
                    var part = parts[j];

                    graphics.strokeRect(
                            part.bounds.min.x,
                            part.bounds.min.y,
                            part.bounds.max.x - part.bounds.min.x,
                            part.bounds.max.y - part.bounds.min.y
                    );
                }
            }
        }

        return this;
    }*/

    /**
     * Renders either all axes, or a single axis indicator, for an array of Bodies, to the given Graphics instance.
     *
     * The debug renderer calls this method if the `showAxes` or `showAngleIndicator` config values are set.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render bounds to your own Graphics instance.
     *
     * @method Physics..World#renderBodyAxes
     * @since 3.22.0
     *
     * @param {array} bodies - An array of bodies from the localWorld.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {boolean} showAxes - If `true` it will render all body axes. If `false` it will render a single axis indicator.
     * @param {number} lineColor - The line color.
     * @param {number} lineOpacity - The line opacity, between 0 and 1.
     */
    /*renderBodyAxes: function (bodies, graphics, showAxes, lineColor, lineOpacity)
    {
        graphics.lineStyle(1, lineColor, lineOpacity);

        for (var i = 0; i < bodies.length; i++)
        {
            var body = bodies[i];
            var parts = body.parts;

            //  1) Don't show invisible bodies
            if (!body.render.visible)
            {
                continue;
            }

            var part;
            var j;
            var k;

            if (showAxes)
            {
                for (j = parts.length > 1 ? 1 : 0; j < parts.length; j++)
                {
                    part = parts[j];

                    for (k = 0; k < part.axes.length; k++)
                    {
                        var axis = part.axes[k];

                        graphics.lineBetween(
                                part.position.x,
                                part.position.y,
                                part.position.x + axis.x * 20,
                                part.position.y + axis.y * 20
                        );
                    }
                }
            }
            else
            {
                for (j = parts.length > 1 ? 1 : 0; j < parts.length; j++)
                {
                    part = parts[j];

                    for (k = 0; k < part.axes.length; k++)
                    {
                        graphics.lineBetween(
                                part.position.x,
                                part.position.y,
                                (part.vertices[0].x + part.vertices[part.vertices.length - 1].x) / 2,
                                (part.vertices[0].y + part.vertices[part.vertices.length - 1].y) / 2
                        );
                    }
                }
            }
        }

        return this;
    }*/

    /**
     * Renders a velocity indicator for an array of Bodies, to the given Graphics instance.
     *
     * The debug renderer calls this method if the `showVelocity` config value is set.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render bounds to your own Graphics instance.
     *
     * @method Physics..World#renderBodyVelocity
     * @since 3.22.0
     *
     * @param {array} bodies - An array of bodies from the localWorld.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {number} lineColor - The line color.
     * @param {number} lineOpacity - The line opacity, between 0 and 1.
     * @param {number} lineThickness - The line thickness.
     */
    /*renderBodyVelocity: function (bodies, graphics, lineColor, lineOpacity, lineThickness)
    {
        graphics.lineStyle(lineThickness, lineColor, lineOpacity);

        for (var i = 0; i < bodies.length; i++)
        {
            var body = bodies[i];

            //  1) Don't show invisible bodies
            if (!body.render.visible)
            {
                continue;
            }

            graphics.lineBetween(
                    body.position.x,
                    body.position.y,
                    body.position.x + (body.position.x - body.positionPrev.x) * 2,
                    body.position.y + (body.position.y - body.positionPrev.y) * 2
            );
        }

        return this;
    }*/

    /**
     * Renders the given array of Bodies to the debug graphics instance.
     *
     * Called automatically by the `postUpdate` method.
     *
     * @method Physics..World#renderBodies
     * @private
     * @since 3.14.0
     *
     * @param {array} bodies - An array of bodies from the localWorld.
     */
    private void renderBodies(List<Body> bodies, int lineColor, float lineOpacity) {
        //for (Body body : bodies) {
        for (int i = 0; i < bodies.size(); i++) {
            Body body = bodies.get(i);

            this.renderBody(body, true, lineColor, lineOpacity);

            /*var partsLength = body.parts.length;

            if (showConvexHulls && partsLength > 1)
            {
                this.renderConvexHull(body, graphics, hullColor, lineThickness);
            }*/
        }
    }

    /**
     * Renders a single  Body to the given  Graphics Game Object.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render a Body to your own Graphics instance.
     *
     * If you don't wish to render a line around the body, set the `lineColor` parameter to `null`
     * Equally, if you don't wish to render a fill, set the `fillColor` parameter to `null`
     *
     * @method Physics..World#renderBody
     * @since 3.22.0
     *
     * @param {BodyType} body - The  Body to be rendered.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {boolean} showInternalEdges - Render internal edges of the polygon?
     * @param {number} [lineColor] - The line color.
     * @param {number} [lineOpacity] - The line opacity, between 0 and 1.
     * @param {number} [lineThickness=1] - The line thickness.
     * @param {number} [fillColor] - The fill color.
     * @param {number} [fillOpacity] - The fill opacity, between 0 and 1.
     *
     * @return {this} This  World instance for method chaining.
     */
    private void renderBody(Body body, boolean showInternalEdges, int lineColor, float lineOpacity) {
        //  Handle compound parts
        List<Body> parts = body.parts;
        int partsLength = parts.size();

        for (int k = (partsLength > 1) ? 1 : 0; k < partsLength; k++) {
            Body part = parts.get(k);

            //  Part polygon
            float circleRadius = part.circleRadius;

            if (circleRadius > 0f) {
                graphics.drawCircle(part.position.x, part.position.y, circleRadius, 0f, 1, false, lineColor, lineOpacity);
            } else {
                List<Vertex> vertices = part.vertices;
                // Quito los vértices internos
                /*List<Vertex> vertices = new ArrayList<>(part.vertices);

                for(Iterator<Vertex> iterator = vertices.iterator(); iterator.hasNext();) {
                    if(iterator.next().isInternal)
                        iterator.remove();
                }*/

                graphics.drawPoly(vertices, true, lineColor, lineOpacity);
            }
        }
    }

    /**
     * Renders the Convex Hull for a single  Body to the given  Graphics Game Object.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render a Body hull to your own Graphics instance.
     *
     * @method Physics..World#renderConvexHull
     * @since 3.22.0
     *
     * @param {BodyType} body - The  Body to be rendered.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {number} hullColor - The color used to render the hull.
     * @param {number} [lineThickness=1] - The hull line thickness.
     *
     * @return {this} This  World instance for method chaining.
     */
    /*renderConvexHull: function (body, graphics, hullColor, lineThickness)
    {
        if (lineThickness === undefined) { lineThickness = 1; }

        var parts = body.parts;
        var partsLength = parts.length;

        //  Render Convex Hulls
        if (partsLength > 1)
        {
            var verts = body.vertices;

            graphics.lineStyle(lineThickness, hullColor);

            graphics.beginPath();

            graphics.moveTo(verts[0].x, verts[0].y);

            for (var v = 1; v < verts.length; v++)
            {
                graphics.lineTo(verts[v].x, verts[v].y);
            }

            graphics.lineTo(verts[0].x, verts[0].y);

            graphics.strokePath();
        }

        return this;
    }*/

    /**
     * Renders all of the constraints in the world (unless they are specifically set to invisible).
     *
     * Called automatically by the `postUpdate` method.
     *
     * @method Physics..World#renderJoints
     * @private
     * @since 3.14.0
     */
    /*renderJoints: function ()
    {
        var graphics = this.debugGraphic;

        // Render constraints
        var constraints = Composite.allConstraints(this.localWorld);

        for (var i = 0; i < constraints.length; i++)
        {
            var config = constraints[i].render;

            var lineColor = config.lineColor;
            var lineOpacity = config.lineOpacity;
            var lineThickness = config.lineThickness;
            var pinSize = config.pinSize;
            var anchorColor = config.anchorColor;
            var anchorSize = config.anchorSize;

            this.renderConstraint(constraints[i], graphics, lineColor, lineOpacity, lineThickness, pinSize, anchorColor, anchorSize);
        }
    }*/

    /**
     * Renders a single  Constraint, such as a Pin or a Spring, to the given  Graphics Game Object.
     *
     * This method is used internally by the  Debug Renderer, but is also exposed publically should
     * you wish to render a Constraint to your own Graphics instance.
     *
     * @method Physics..World#renderConstraint
     * @since 3.22.0
     *
     * @param {ConstraintType} constraint - The  Constraint to render.
     * @param {GameObjects.Graphics} graphics - The Graphics object to render to.
     * @param {number} lineColor - The line color.
     * @param {number} lineOpacity - The line opacity, between 0 and 1.
     * @param {number} lineThickness - The line thickness.
     * @param {number} pinSize - If this constraint is a pin, this sets the size of the pin circle.
     * @param {number} anchorColor - The color used when rendering this constraints anchors. Set to `null` to not render anchors.
     * @param {number} anchorSize - The size of the anchor circle, if this constraint has anchors and is rendering them.
     *
     * @return {this} This  World instance for method chaining.
     */
    /*renderConstraint: function (constraint, graphics, lineColor, lineOpacity, lineThickness, pinSize, anchorColor, anchorSize)
    {
        var render = constraint.render;

        if (!render.visible || !constraint.pointA || !constraint.pointB)
        {
            return this;
        }

        graphics.lineStyle(lineThickness, lineColor, lineOpacity);

        var bodyA = constraint.bodyA;
        var bodyB = constraint.bodyB;
        var start;
        var end;

        if (bodyA)
        {
            start = Vector.add(bodyA.position, constraint.pointA);
        }
        else
        {
            start = constraint.pointA;
        }

        if (render.type === 'pin')
        {
            graphics.strokeCircle(start.x, start.y, pinSize);
        }
        else
        {
            if (bodyB)
            {
                end = Vector.add(bodyB.position, constraint.pointB);
            }
            else
            {
                end = constraint.pointB;
            }

            graphics.beginPath();
            graphics.moveTo(start.x, start.y);

            if (render.type === 'spring')
            {
                var delta = Vector.sub(end, start);
                var normal = Vector.perp(Vector.normalise(delta));
                var coils = Math.ceil(Common.clamp(constraint.length / 5, 12, 20));
                var offset;

                for (var j = 1; j < coils; j += 1)
                {
                    offset = (j % 2 === 0) ? 1 : -1;

                    graphics.lineTo(
                            start.x + delta.x * (j / coils) + normal.x * offset * 4,
                            start.y + delta.y * (j / coils) + normal.y * offset * 4
                    );
                }
            }

            graphics.lineTo(end.x, end.y);
        }

        graphics.strokePath();

        if (render.anchors && anchorSize > 0)
        {
            graphics.fillStyle(anchorColor);
            graphics.fillCircle(start.x, start.y, anchorSize);
            graphics.fillCircle(end.x, end.y, anchorSize);
        }

        return this;
    }*/

    /**
     * Applys `Body.update` to all given `bodies`
     * @method _bodiesUpdate
     * @private
     * @param {body[]} bodies
     * @param {number} deltaTime
     * The amount of time elapsed between updates
     * @param {number} timeScale
     * @param {number} correction
     * The Verlet correction factor (deltaTime / lastDeltaTime)
     * @param {bounds} worldBounds
     */
    private void bodiesUpdate(float deltaTime) {
        //for (Body body : this.bodies) {
        for (int i = 0; i < this.bodies.size(); i++) {
            Body body = this.bodies.get(i);

            if (body.isStatic || body.isSleeping)
                continue;

            body.update(deltaTime);
        }
    }


    /*public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }*/
}
