package test.collision;

import java.util.ArrayList;
import java.util.List;

/**
 * The `Pairs` feature contains methods for creating and manipulating collision pair sets.
 *
 * @class Pairs
 */
public class PairsTest {

    public List<Pair> table;
    List<Pair> list;
    public List<Pair> collisionStart;
    public List<Pair> collisionActive;
    public List<Pair> collisionEnd;

    public Pairs() {
        this.table = new ArrayList<>();
        this.list = new ArrayList<>();
        this.collisionStart = new ArrayList<>();
        this.collisionActive = new ArrayList<>();
        this.collisionEnd = new ArrayList<>();
    }


    /**
     * Updates pairs given a list of collisions.
     * @method update
     * @param {object} pairs
     * @param {collision[]} collisions
     * @param {number} timestamp
     */
    public void update(List<Collision> collisions, long timestamp) {
        // clear collision state arrays, but maintain old reference
        collisionStart.clear();
        collisionEnd.clear();
        collisionActive.clear();

        //for (Pair pair : list) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).confirmedActive = false;
        }

        //for (Collision collision : collisions) {
        for (int i = 0; i < collisions.size(); i++) {
            Collision collision = collisions.get(i);

            if (collision.collided) {
                String pairId = Pair.id(collision.bodyA, collision.bodyB);
                Pair pair = Pairs.getById(table, pairId);

                if (pair != null) {
                    // pair already exists (but may or may not be active)
                    if (pair.isActive) {
                        // pair exists and is active
                        collisionActive.add(pair);
                    } else {
                        // pair exists but was inactive, so a collision has just started again
                        collisionStart.add(pair);
                    }

                    // update the pair
                    pair.update(collision, timestamp);
                    pair.confirmedActive = true;
                } else {
                    // pair did not exist, create a new pair
                    pair = new Pair(collision, timestamp);
                    Pairs.setById(table, pairId, pair);

                    // push the new pair
                    collisionStart.add(pair);
                    list.add(pair);
                }
            }
        }

        // deactivate previously active pairs that are now inactive
        //for (Pair pair : list) {
        for (int i = 0; i < list.size(); i++) {
            Pair pair = list.get(i);

            if (pair.isActive && !pair.confirmedActive) {
                pair.setActive(false, timestamp);
                collisionEnd.add(pair);
            }
        }
    }

    /**
     * Finds and removes pairs that have been inactive for a set amount of time.
     * @method removeOld
     * @param {object} pairs
     * @param {number} timestamp
     */
    public void removeOld(long timestamp) {
        List<Integer> indexesToRemove = new ArrayList();

        for (int i = 0; i < list.size(); i++) {
            Pair pair = list.get(i);
            Collision collision = pair.collision;

            // never remove sleeping pairs
            if (collision.bodyA.isSleeping || collision.bodyB.isSleeping) {
                pair.timeUpdated = timestamp;
                continue;
            }

            // if pair is inactive for too long, mark it to be removed
            if (timestamp - pair.timeUpdated > Pair.MAX_IDLE_LIFE) {
                indexesToRemove.add(i);
            }
        }

        // remove marked pairs
        for (int i = 0; i < indexesToRemove.size(); i++) {
            int pairIndex = indexesToRemove.get(i) - i;
            Pair pair = list.get(pairIndex);
            table.remove(pair);
            list.remove(pairIndex);
        }
    }

    /**
     * Clears the given pairs structure.
     * @method clear
     * @param {pairs} pairs
     * @return {pairs} pairs
     */
    public Pairs clear() {
        this.table.clear();
        this.list.clear();
        this.collisionStart.clear();
        this.collisionActive.clear();
        this.collisionEnd.clear();
        return this;
    }

    public static void setById(List<Pair> pairs, String id, Pair newPair) {
        for (int i = 0; i < pairs.size(); i++) {
            Pair pair = pairs.get(i);
            if (pair.id.equals(id)) {
                pairs.set(i, newPair);
                break;
            }
        }
    }

    public static Pair getById(List<Pair> pairs, String id) {
        //for (Pair pair : pairs) {
        for (int i = 0; i < pairs.size(); i++) {
            Pair pair = pairs.get(i);

            if (pair.id.equals(id)) {
                return pair;
            }
        }
        return null;
    }

    public static List<Pair> getAllById(List<Pair> pairs, String id) {
        List<Pair> found = new ArrayList<>();
        //for (Pair pair : pairs) {
        for (int i = 0; i < pairs.size(); i++) {
            Pair pair = pairs.get(i);

            if (pair.id.equals(id)) {
                found.add(pair);
            }
        }

        return found;
    }


}
