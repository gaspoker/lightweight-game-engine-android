package test.collision;

import sdk.core.Body;
import sdk.geometry.Vertex;

import java.util.ArrayList;
import java.util.List;

/**
 * The `Pair` feature contains methods for creating and manipulating collision pairs.
 *
 * @class Pair
 */
public class PairTest {
    public static final int MAX_IDLE_LIFE = 1000;

    String id;
    public Body bodyA;
    public Body bodyB;
    List<Vertex.Contact> activeContacts;
    float separation;
    boolean isActive;
    boolean confirmedActive;
    boolean isSensor;
    long timeCreated;
    long timeUpdated;
    Collision collision;
    float inverseMass;
    float friction;
    float frictionStatic;
    float restitution;
    float slop;
    public int count;

    public Pair(Body bodyA, Body bodyB) {
        this.id = id(bodyA, bodyB);
        this.activeContacts = new ArrayList();
        this.separation = 0;
        this.isActive = true;
        this.confirmedActive = true;
        this.isSensor = bodyA.isSensor || bodyB.isSensor;
        this.timeCreated = System.nanoTime(); //System.currentTimeMillis();
        this.timeUpdated = System.nanoTime(); //System.currentTimeMillis();
        this.collision = null;
        this.inverseMass = 0;
        this.friction = 0;
        this.frictionStatic = 0;
        this.restitution = 0;
        this.slop = 0;
    }

    public Pair(Collision collision, long timestamp) {
        this(collision.bodyA, collision.bodyB);
        this.bodyA = collision.bodyA;
        this.bodyB = collision.bodyB;
        this.count = 0;
        update(collision, timestamp);
    }

    public Pair(Body bodyA, Body bodyB, int count) {
        this(bodyA, bodyB);
        this.bodyA = bodyA;
        this.bodyB = bodyB;
        this.count = count;
    }

    /**
     * Updates a pair given a collision.
     * @method update
     * @param {pair} pair
     * @param {collision} collision
     * @param {number} timestamp
     */
    public void update(Collision collision, long timestamp) {
        this.collision = collision;

        if (collision.collided) {
            List<Vertex> supports = collision.supports;
            Body parentA = collision.parentA;
            Body parentB = collision.parentB;

            inverseMass = parentA.inverseMass + parentB.inverseMass;
            friction = Math.min(parentA.friction, parentB.friction);
            frictionStatic = Math.max(parentA.frictionStatic, parentB.frictionStatic);
            restitution = Math.max(parentA.restitution, parentB.restitution);
            slop = Math.max(parentA.slop, parentB.slop);

            for (int i = 0; i < supports.size(); i++) {
                activeContacts.add(supports.get(i).contact);
            }

            // optimise array size
            int supportCount = supports.size();
            if (supportCount < activeContacts.size()) {
                int size = activeContacts.size();
                activeContacts.subList(supportCount, size).clear();
            }

            separation = collision.depth;
            setActive(true, timestamp);
        } else {
            if (isActive) {
                setActive(false, timestamp);
            }
        }
    }

    /**
     * Set a pair as active or inactive.
     * @method setActive
     * @param {pair} pair
     * @param {bool} isActive
     * @param {number} timestamp
     */
    public void setActive(boolean isActive, long timestamp) {
        if (isActive) {
            this.isActive = true;
            this.timeUpdated = timestamp;
        } else {
            this.isActive = false;
        }
    }

    /**
     * Get the id for the given pair.
     * @method id
     * @param {body} bodyA
     * @param {body} bodyB
     * @return {string} Unique pairId
     */
    public static String id(Body bodyA, Body bodyB) {
        if (bodyA.id < bodyB.id) {
            return "A" + bodyA.id + "B" + bodyB.id;
        } else {
            return "A" + bodyB.id + "B" + bodyA.id;
        }
    }


}
