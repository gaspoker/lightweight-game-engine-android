package test.collision;

import android.util.Log;

import sdk.core.Body;
import sdk.geometry.Bounds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The `Grid` feature contains methods for creating and manipulating collision broadphase grid structures.
 *
 * @class Grid
 */
public class GridTest {
    public Detector detector;
    Map<String, List<Body>> buckets;
    Map<String, Pair> pairs;
    public List<Pair> pairsList;
    float bucketWidth;
    float bucketHeight;
    Collider collider;

    public Grid(Collider collider) {
        this.collider = collider;
        this.buckets = new HashMap<>();
        this.pairs = new HashMap<>();
        this.pairsList = new ArrayList<>();
        this.detector = new Detector(this.collider);
        this.bucketWidth = 48;
        this.bucketHeight = 48;
    }

    /**
     * Updates the grid.
     * @method update
     * @param {grid} grid
     * @param {body[]} bodies
     * @param {engine} engine
     * @param {boolean} forceUpdate
     */
    public void update(List<Body> bodies, boolean forceUpdate) {
        int col, row;
//                world = engine.world,
        //buckets = grid.buckets,
        List<Body> bucket;
        String bucketId;
        boolean gridChanged = false;

        //for (Body body : bodies) {
        for (int i = 0; i < bodies.size(); i++) {
            Body body = bodies.get(i);

            if (body.isSleeping && !forceUpdate)
                continue;

            // don't update out of world bodies
            if (body.bounds.max.x < this.collider.bounds.min.x || body.bounds.min.x > this.collider.bounds.max.x
                    || body.bounds.max.y < this.collider.bounds.min.y || body.bounds.min.y > this.collider.bounds.max.y)
                continue;

            Region newRegion = getRegion(body);

            // if the body has changed grid region
            if (body.region == null || !newRegion.id.equals(body.region.id) || forceUpdate) {

                if (body.region == null || forceUpdate) {
                    body.region = newRegion;
                }

                Region union = regionUnion(newRegion, body.region);

                // update grid buckets affected by region change
                // iterate over the union of both regions
                for (col = union.startCol; col <= union.endCol; col++) {
                    for (row = union.startRow; row <= union.endRow; row++) {
                        bucketId = getBucketId(col, row);
                        bucket = this.buckets.get(bucketId);

                        boolean isInsideNewRegion = (col >= newRegion.startCol && col <= newRegion.endCol
                                && row >= newRegion.startRow && row <= newRegion.endRow);

                        boolean isInsideOldRegion = (col >= body.region.startCol && col <= body.region.endCol
                                && row >= body.region.startRow && row <= body.region.endRow);

                        // remove from old region buckets
                        if (!isInsideNewRegion && isInsideOldRegion) {
                            if (isInsideOldRegion) {
                                if (bucket != null)
                                    bucketRemoveBody(bucket, body);
                            }
                        }

                        // add to new region buckets
                        if (body.region == newRegion || (isInsideNewRegion && !isInsideOldRegion) || forceUpdate) {
                            if (bucket == null) {
                                bucket = createBucket(bucketId);
                            }
                            bucketAddBody(bucket, body);
                        }
                    }
                }

                // set the new region
                body.region = newRegion;

                // flag changes so we can update pairs
                gridChanged = true;
            }
        }

        // update pairs list only if pairs changed (i.e. a body changed region)
        if (gridChanged) {
            this.pairsList = createActivePairsList();
        }
    }

    /**
     * Clears the grid.
     * @method clear
     * @param {grid} grid
     */
    public void clear() {
        this.buckets.clear();
        this.pairs.clear();
        this.pairsList.clear();
    }

    /**
     * Finds the union of two regions.
     * @method _regionUnion
     * @private
     * @param {} regionA
     * @param {} regionB
     * @return {} region
     */
    private Region regionUnion(Region regionA, Region regionB) {
        int startCol = Math.min(regionA.startCol, regionB.startCol);
        int endCol = Math.max(regionA.endCol, regionB.endCol);
        int startRow = Math.min(regionA.startRow, regionB.startRow);
        int endRow = Math.max(regionA.endRow, regionB.endRow);

        return createRegion(startCol, endCol, startRow, endRow);
    }

    /**
     * Gets the region a given body falls in for a given grid.
     * @method _getRegion
     * @private
     * @param {} grid
     * @param {} body
     * @return {} region
     */
    private Region getRegion(Body body) {
        Bounds bounds = body.bounds;

        //Log.i("COLLIDER-TEST", "min: {x:" + body.bounds.min.x + ", y:" + body.bounds.min.y + "}");
        //Log.i("COLLIDER-TEST", "max: {x:" + body.bounds.max.x + ", y:" + body.bounds.max.y + "}");

        int startCol = (int) Math.floor(bounds.min.x / this.bucketWidth);
        int endCol = (int) Math.floor(bounds.max.x / this.bucketWidth);
        int startRow = (int) Math.floor(bounds.min.y / this.bucketHeight);
        int endRow = (int) Math.floor(bounds.max.y / this.bucketHeight);

        return createRegion(startCol, endCol, startRow, endRow);
    }

    /**
     * Creates a region.
     * @method _createRegion
     * @private
     * @param {} startCol
     * @param {} endCol
     * @param {} startRow
     * @param {} endRow
     * @return {} region
     */
    private Region createRegion(int startCol, int endCol, int startRow, int endRow) {
        return new Region(startCol, endCol, startRow, endRow);
    }

    /**
     * Gets the bucket id at the given position.
     * @method _getBucketId
     * @private
     * @param {} column
     * @param {} row
     * @return {string} bucket id
     */
    private String getBucketId(int column, int row) {
        return "C" + column + "R" + row;
    }

    /**
     * Creates a bucket.
     * @method _createBucket
     * @private
     * @param {} buckets
     * @param {} bucketId
     * @return {} bucket
     */
    private List<Body> createBucket(String bucketId) {
        List<Body> bucket = new ArrayList<>();
        this.buckets.put(bucketId, bucket);
        return this.buckets.get(bucketId);
    }

    /**
     * Adds a body to a bucket.
     * @method _bucketAddBody
     * @private
     * @param {} grid
     * @param {} bucket
     * @param {} body
     */
    private void bucketAddBody(List<Body> bucket, Body body) {
        // add new pairs
        for (int i = 0; i < bucket.size(); i++) {
            Body bodyB = bucket.get(i);

            if (body.id == bodyB.id || (body.isStatic && bodyB.isStatic))
                continue;

            // keep track of the number of buckets the pair exists in
            // important for Grid.update to work
            String pairId = Pair.id(body, bodyB);
            Pair pair = this.pairs.get(pairId);

            if (pair != null) {
                pair.count++;
            } else {
                pair = new Pair(body, bodyB, 1);
                this.pairs.put(pairId, pair);
            }
        }

        // add to bodies (after pairs, otherwise pairs with self)
        bucket.add(body);
    }

    /**
     * Removes a body from a bucket.
     * @method _bucketRemoveBody
     * @private
     * @param {} grid
     * @param {} bucket
     * @param {} body
     */
    private void bucketRemoveBody(List<Body> bucket, Body body) {
        // remove from bucket
        bucket.remove(body);

        // update pair counts
        for (int i = 0; i < bucket.size(); i++) {
            // keep track of the number of buckets the pair exists in
            // important for createActivePairsList to work
            Body bodyB = bucket.get(i);
            String pairId = Pair.id(body, bodyB);
            Pair pair = this.pairs.get(pairId);

            if (pair != null) {
                pair.count--;
            }
        }
    }

    /**
     * Generates a list of the active pairs in the grid.
     * @method _createActivePairsList
     * @private
     * @param {} grid
     * @return [] pairs
     */
    private List<Pair> createActivePairsList() {
        List<Pair> pairs = new ArrayList<>();

        Set<String> pairKeys = new HashSet<>(this.pairs.keySet());

        for (String key : pairKeys) {
            Pair pair = this.pairs.get(key);

            // if pair exists in at least one bucket
            // it is a pair that needs further collision testing so push it
            if (pair.count > 0) {
                pairs.add(pair);
            } else {
                this.pairs.remove(key);
            }
        }

        return pairs;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Internal Classes
    ////////////////////////////////////////////////////////////////////////////////////////////////
    public class Region {
        String id;
        int startCol, endCol, startRow, endRow;

        Region(int startCol, int endCol, int startRow, int endRow) {
            this.id = startCol + "," + endCol + "," + startRow + "," + endRow;
            this.startCol = startCol;
            this.endCol = endCol;
            this.startRow = startRow;
            this.endRow = endRow;
        }
    }


}












