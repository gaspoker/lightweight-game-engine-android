package test.geometry;

public class RectangleTest {
    public Vector lowerLeft;
    public float width, height;
    
    public Rectangle(float x, float y, float width, float height) {
        set(x, y, width, height);
    }

    public void set(float x, float y, float width, float height) {
        this.lowerLeft = new Vector(x,y);
        this.width = width;
        this.height = height;
    }

    public static Rectangle add(Rectangle rect, float dx, float dy) {
        Rectangle output = new Rectangle(rect.lowerLeft.x, rect.lowerLeft.y, rect.width, rect.height);
        output.lowerLeft.add(dx, dy);
        return output;
    }

    /*public void addX(float dx) {
        this.lowerLeft.add(dx, 0);
    }

    public void addY(float dy) {
        this.lowerLeft.add(0, dy);
    }*/
}
