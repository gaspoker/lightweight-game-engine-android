package test.geometry;

public class InterpolationTest {

    /**
     * A linear interpolation method.
     *
     * @function Math.Interpolation.Linear
     * @since 3.0.0
     * @see {@link //en.wikipedia.org/wiki/Linear_interpolation}
     *
     * @param {number[]} v - The input array of values to interpolate between.
     * @param {!number} k - The percentage of interpolation, between 0 and 1.
     *
     * @return {!number} The interpolated value.
    */
    static public float getLinearValue(short[] v, float k) {
        int m = v.length - 1;
        float f = m * k;
        int i = (int) Math.floor(f);

        if (k < 0f) {
            return linear(v[0], v[1], f);
        } else if (k > 1f) {
            return linear(v[m], v[m - 1], m - f);
        } else {
            return linear(v[i], v[Math.min(i + 1, m)], f - i);
        }
    }

    /**
     * A bezier interpolation method.
     *
     * @function Math.Interpolation.Bezier
     * @since 3.0.0
     *
     * @param {number[]} v - The input array of values to interpolate between.
     * @param {number} k - The percentage of interpolation, between 0 and 1.
     *
     * @return {number} The interpolated value.
     */
    static public float getBezierValue(int[] v, float k) {
        float b = 0f;
        int n = v.length - 1;

        for (int i = 0; i <= n; i++) {
            b += Math.pow(1f - k, n - i) * Math.pow(k, i) * v[i] * bernstein(n, i);
        }

        return b;
    }

    /**
     * Calculates the factorial of a given number for integer values greater than 0.
     *
     * @function Math.Factorial
     * @since 3.0.0
     *
     * @param {number} value - A positive integer to calculate the factorial of.
     *
     * @return {number} The factorial of the given number.
     */
    static private int factorial(int value) {
        if (value == 0) {
            return 1;
        }

        int res = value;

        while (--value != 0) {
            res *= value;
        }

        return res;
    }

    /**
     * Calculates a linear (interpolation) value over t.
     *
     * @function Math.Linear
     * @since 3.0.0
     *
     * @param {number} p0 - The first point.
     * @param {number} p1 - The second point.
     * @param {number} t - The percentage between p0 and p1 to return, represented as a number between 0 and 1.
     *
     * @return {number} The step t% of the way between p0 and p1.
     */
    static private float linear(float p0, float p1, float t) {
        return (p1 - p0) * t + p0;
    }

    /**
     * [description]
     *
     * @function Math.Bernstein
     * @since 3.0.0
     *
     * @param {number} n - [description]
     * @param {number} i - [description]
     *
     * @return {number} [description]
     */
    static private float bernstein(int n, int i) {
        return (float) factorial(n) / factorial(i) / factorial(n - i);
    }


}
