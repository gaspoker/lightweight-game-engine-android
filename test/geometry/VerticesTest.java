package test.geometry;

import sdk.core.Body;
import sdk.geometry.Vertex.Contact;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The `Vertices` feature contains methods for creating and manipulating sets of vertices.
 * A set of vertices is an array of `Vector` with additional indexing properties inserted by `Vertices.create`
 * A `Body` maintains a set of vertices to represent the shape of the object (its convex hull).
 *
 * @class Vertices
 */
public class VerticesTest {

    /**
     * Creates a new set of `Body` compatible vertices.
     * The `points` argument accepts an array of `Vector` points orientated around the origin `(0, 0)`, for example:
     *
     *     [{ x: 0, y: 0 }, { x: 25, y: 50 }, { x: 50, y: 0 }]
     *
     * The `Vertices.create` method returns a new array of vertices, which are similar to .Vector objects,
     * but with some additional references required for efficient collision detection routines.
     *
     * Vertices must be specified in clockwise order.
     *
     * Note that the `body` argument is not optional, a `Body` reference must be provided.
     *
     * @method create
     * @param {vector[]} points
     * @param {body} body
     */
    public static List<Vertex> fromVectors(List<? extends Vector> points, Body body) { // Vertices.create en .js
        List<Vertex> vertices = new ArrayList<>();
        for (int i = 0; i < points.size(); i++) {
            Vertex vertex = new Vertex(points.get(i));
            vertex.index = i;
            vertex.body = body;
            vertex.isInternal = false;
            vertex.contact = new Contact(vertex, 0f, 0f);
            vertex.offset = null;

            vertices.add(vertex);
        }
        return vertices;
    }

    /**
     * Parses a string containing ordered x y pairs separated by spaces (and optionally commas),
     * into a `Vertices` object for the given `Body`
     * For parsing SVG paths, see `Svg.pathToVertices`
     * @method fromPath
     * @param {string} path
     * @param {body} body
     * @return {vertices} vertices
     */
    // Fuente:
    //      https://regex101.com/codegen?language=java
    //      http://tutorials.jenkov.com/java-regex/matcher.html
    public static List<Vertex> fromPath(String path, Body body) {
        final String pathPattern = "L?\\s*([-\\d.e]+)[\\s,]*([-\\d.e]+)*";

        final Pattern pattern = Pattern.compile(pathPattern, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        final Matcher matcher = pattern.matcher(path);

        List<Vector> points = new ArrayList<>();

        while (matcher.find()) {
            float x = Float.parseFloat(matcher.group(1));
            float y = Float.parseFloat(matcher.group(2));
            points.add(new Vector(x, y));
        }

        return fromVectors(points, body);
    }

    /**
     * Returns the centre (centroid) of the set of vertices.
     * @method centre
     * @param {vertices} vertices
     * @return {vector} The centre point
     */
    public static Vector centre(List<Vertex> vertices) {
        float area = Vertices.area(vertices, true);

        Vector centre = new Vector(0f,0f);

        for (int i = 0; i < vertices.size(); i++) {
            int j = (i + 1) % vertices.size();
            float cross = Vector.cross(vertices.get(i), vertices.get(j));

            Vector temp = Vector.mult(Vertex.add(vertices.get(i), vertices.get(j), null), cross);

            centre = Vector.add(centre, temp, null);
        }

        return Vector.div(centre, 6f * area);
    }

    /**
     * Returns the average (mean) of the set of vertices.
     * @method mean
     * @param {vertices} vertices
     * @return {vertex} The average point
     */
    static Vertex mean(List<Vertex> vertices) {
        Vertex average = new Vertex(0f,0f);

        //for (Vertex vertex : vertices) {
        for (int i = 0; i < vertices.size(); i++) {
            Vertex vertex = vertices.get(i);

            //average.add(vertex);
            average.x += vertex.x;
            average.y += vertex.y;
        }
        average.div(vertices.size());

        return average;
    }

    /**
     * Returns the area of the set of vertices.
     * @method area
     * @param {vertices} vertices
     * @param {bool} signed (false por defecto)
     * @return {number} The area
     */
    public static float area(List<Vertex> vertices) {
        return area(vertices, false);
    }

    public static float area(List<Vertex> vertices, boolean signed) {
        float area = 0f;
        int j = vertices.size() - 1;
        for (int i = 0; i < vertices.size(); i++) {
            area += (vertices.get(j).x - vertices.get(i).x) * (vertices.get(j).y + vertices.get(i).y);
            j = i;
        }

        if (signed) {
            return area / 2f;
        }

        return Math.abs(area) / 2f;
    }

    /**
     * Returns the moment of inertia (second moment of area) of the set of vertices given the total mass.
     * @method inertia
     * @param {vertices} vertices
     * @param {number} mass
     * @return {number} The polygon's moment of inertia
     */
    public static float inertia(List<Vertex> v, float mass) {
        float numerator = 0f;
        float denominator = 0f;

        // find the polygon's moment of inertia, using second moment of area
        // from equations at http://www.physicsforums.com/showthread.php?t=25293
        for (int n = 0; n < v.size(); n++) {
            int j = (n + 1) % v.size();
            float cross = Math.abs(Vector.cross(v.get(j), v.get(n)));
            numerator += cross * (Vector.dot(v.get(j), v.get(j)) + Vector.dot(v.get(j), v.get(n)) + Vector.dot(v.get(n), v.get(n)));
            denominator += cross;
        }

        return (mass / 6f) * (numerator / denominator);
    }

    /**
     * Translates the set of vertices in-place.
     * @method translate
     * @param {vertices} vertices
     * @param {vector} vector
     * @param {number} scalar
     */
    public static List<Vertex> translate(List<Vertex> vertices, Vector vector) {
        return translate(vertices, vector, null);
    }

    public static List<Vertex> translate(List<Vertex> vertices, Vector vector, Float scalar) {
        if (scalar != null) {

            //for (Vertex vertex : vertices) {
            for (int i = 0; i < vertices.size(); i++) {
                Vertex vertex = vertices.get(i);
                vertex.x += vector.x * scalar;
                vertex.y += vector.y * scalar;
            }
        } else {
            //for (Vertex vertex : vertices) {
            for (int i = 0; i < vertices.size(); i++) {
                Vertex vertex = vertices.get(i);
                vertex.x += vector.x;
                vertex.y += vector.y;
            }
        }

        return vertices;
    }

    /**
     * Rotates the set of vertices in-place.
     * @method rotate
     * @param {vertices} vertices
     * @param {number} angle
     * @param {vector} point
     */
    public static List<Vertex> rotate(List<Vertex> vertices, float angle, Vector point) {
        if (angle == 0f)
            return null;

        float cos = (float) Math.cos(angle);
        float sin = (float) Math.sin(angle);

        //for (Vertex vertex : vertices) {
        for (int i = 0; i < vertices.size(); i++) {
            Vertex vertex = vertices.get(i);

            float dx = vertex.x - point.x;
            float dy = vertex.y - point.y;

            vertex.x = point.x + (dx * cos - dy * sin);
            vertex.y = point.y + (dx * sin + dy * cos);
        }

        return vertices;
    }

    /**
     * Returns `true` if the `point` is inside the set of `vertices`
     * @method contains
     * @param {vertices} vertices
     * @param {vector} point
     * @return {boolean} True if the vertices contains point, otherwise false
     */
    public static boolean contains(List<Vertex> vertices, Vertex point) {
        for (int i = 0; i < vertices.size(); i++) {
            Vertex vertex = vertices.get(i);
            Vertex nextVertice = vertices.get((i + 1) % vertices.size());

            if ((point.x - vertex.x) * (nextVertice.y - vertex.y) + (point.y - vertex.y) * (vertex.x - nextVertice.x) > 0f) {
                return false;
            }
        }

        return true;
    }


    /**
     * Scales the vertices from a point (default is centre) in-place.
     * @method scale
     * @param {vertices} vertices
     * @param {number} scaleX
     * @param {number} scaleY
     * @param {vector} point
     */
    public static List<Vertex> scale(List<Vertex> vertices, float scaleX, float scaleY, Vector point) {
        if (scaleX == 1f && scaleY == 1f)
            return vertices;

        if (point == null) {
            point = Vertices.centre(vertices);
        }

        //for (Vertex vertex : vertices) {
        for (int i = 0; i < vertices.size(); i++) {
            Vertex vertex = vertices.get(i);

            Vector delta = Vector.sub(vertex, point, null);
            vertex.x = point.x + delta.x * scaleX;
            vertex.y = point.y + delta.y * scaleY;
        }

        return vertices;
    }

    /**
     * Chamfers a set of vertices by giving them rounded corners, returns a new set of vertices.
     * The radius parameter is a single number or an array to specify the radius for each vertex.
     * @method chamfer
     * @param {vertices} vertices
     * @param {number[]} radius
     * @param {number} quality
     * @param {number} qualityMin
     * @param {number} qualityMax
     */
    /*Vertices.chamfer = function(vertices, radius, quality, qualityMin, qualityMax) {
        if (typeof radius === 'number') {
            radius = [radius];
        } else {
            radius = radius || [8];
        }

        // quality defaults to -1, which is auto
        quality = (typeof quality !== 'undefined') ? quality : -1;
        qualityMin = qualityMin || 2;
        qualityMax = qualityMax || 14;

        var newVertices = [];

        for (var i = 0; i < vertices.length; i++) {
            var prevVertex = vertices[i - 1 >= 0 ? i - 1 : vertices.length - 1],
                    vertex = vertices[i],
                    nextVertex = vertices[(i + 1) % vertices.length],
                    currentRadius = radius[i < radius.length ? i : radius.length - 1];

            if (currentRadius === 0) {
                newVertices.push(vertex);
                continue;
            }

            var prevNormal = Vector.normalise({
                    x: vertex.y - prevVertex.y,
                    y: prevVertex.x - vertex.x
            });

            var nextNormal = Vector.normalise({
                    x: nextVertex.y - vertex.y,
                    y: vertex.x - nextVertex.x
            });

            var diagonalRadius = Math.sqrt(2 * Math.pow(currentRadius, 2)),
                    radiusVector = Vector.mult(Common.clone(prevNormal), currentRadius),
                    midNormal = Vector.normalise(Vector.mult(Vector.add(prevNormal, nextNormal), 0.5)),
                    scaledVertex = Vector.sub(vertex, Vector.mult(midNormal, diagonalRadius));

            var precision = quality;

            if (quality === -1) {
                // automatically decide precision
                precision = Math.pow(currentRadius, 0.32) * 1.75;
            }

            precision = Common.clamp(precision, qualityMin, qualityMax);

            // use an even value for precision, more likely to reduce axes by using symmetry
            if (precision % 2 === 1)
                precision += 1;

            var alpha = Math.acos(Vector.dot(prevNormal, nextNormal)),
                    theta = alpha / precision;

            for (var j = 0; j < precision; j++) {
                newVertices.push(Vector.add(Vector.rotate(radiusVector, theta * j), scaledVertex));
            }
        }

        return newVertices;
    }*/


    /**
     * Sorts the input vertices into clockwise order in place.
     * @method clockwiseSort
     * @param {vertices} vertices
     * @return {vertices} vertices
     */
    public static class ClockwiseSort implements Comparator<Vertex> {
        Vertex centre;

        public ClockwiseSort(List<Vertex> vertices) {
            // Calculo la media de los vértices
            centre = Vertices.mean(vertices);
        }

        public int compare(Vertex vertexA, Vertex vertexB) {
            float result = Vertex.angle(centre, vertexA) - Vertex.angle(centre, vertexB);
            return Float.compare(result, 0f);
        }
    }


    /**
     * Returns true if the vertices form a convex shape (vertices must be in clockwise order).
     * @method isConvex
     * @param {vertices} vertices
     * @return {bool} `true` if the `vertices` are convex, `false` if not (or `null` if not computable).
     */
    public static Boolean isConvex(List<Vertex>vertices) {
        // http://paulbourke.net/geometry/polygonmesh/
        // Copyright (c) Paul Bourke (use permitted)

        int flag = 0;
        int n = vertices.size();

        if (n < 3) {
            return null;
        }

        for (int i = 0; i < n; i++) {
            int j = (i + 1) % n;
            int k = (i + 2) % n;
            float z = (vertices.get(j).x - vertices.get(i).x) * (vertices.get(k).y - vertices.get(j).y);
            z -= (vertices.get(j).y - vertices.get(i).y) * (vertices.get(k).x - vertices.get(j).x);

            if (z < 0f) {
                flag |= 1;
            } else if (z > 0) {
                flag |= 2;
            }

            if (flag == 3) {
                return false;
            }
        }

        if (flag != 0) {
            return true;
        } else {
            return null;
        }
    }

    /**
     * Returns the convex hull of the input vertices as a new array of points.
     * @method hull
     * @param {vertices} vertices
     * @return [vertex] vertices
     */
    public static List<Vertex> hull(List<Vertex> vertices) {
        // http://geomalgorithms.com/a10-_hull-1.html
        List<Vertex> upper = new ArrayList<>();
        List<Vertex> lower = new ArrayList<>();

        // sort vertices on x-axis (y-axis for ties)
        List<Vertex> cpyVertices = new ArrayList<>(vertices);
        Collections.sort(cpyVertices, new AxisSort());

        // build lower hull
        for (int i = 0; i < cpyVertices.size(); i++) {
            Vertex vertex = cpyVertices.get(i);
            while (lower.size() >= 2 && Vector.cross3(lower.get(lower.size() - 2), lower.get(lower.size() - 1), vertex) <= 0) {
                lower.remove(lower.size() - 1);
            }
            lower.add(vertex);
        }

        // build upper hull
        for (int i = cpyVertices.size() - 1; i >= 0; i--) {
            Vertex vertex = cpyVertices.get(i);
            while (upper.size() >= 2 && Vector.cross3(upper.get(upper.size() - 2), upper.get(upper.size() - 1), vertex) <= 0) {
                upper.remove(upper.size() - 1);
            }
            upper.add(vertex);
        }

        // concatenation of the lower and upper hulls gives the convex hull
        // omit last points because they are repeated at the beginning of the other list
        upper.remove(upper.size() - 1);
        lower.remove(lower.size() - 1);
        upper.addAll(lower);

        return upper;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Internal Classes
    ////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Sort vertices on x-axis (y-axis for ties)
     */
    public static class AxisSort implements Comparator<Vertex> {
        public int compare(Vertex vertexA, Vertex vertexB) {
            float dx = vertexA.x - vertexB.x;
            float result = (dx != 0f ? dx : vertexA.y - vertexB.y);
            return Float.compare(result, 0f);
        }

    }


}
