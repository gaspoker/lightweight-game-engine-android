package test.geometry;

/**
 * @classdesc
 * The Size component allows you to set `width` and `height` properties and define the relationship between them.
 *
 * The component can automatically maintain the aspect ratios between the two values, and clamp them
 * to a defined min-max range. You can also control the dominant axis. When dimensions are given to the Size component
 * that would cause it to exceed its min-max range, the dimensions are adjusted based on the dominant axis.
 *
 * @class Size
 * @memberof Structs
 * @constructor
 * @since 3.16.0
 *
 * @param {number} [width=0] - The width of the Size component.
 * @param {number} [height=width] - The height of the Size component. If not given, it will use the `width`
 * @param {integer} [aspectMode=0] - The aspect mode of the Size component. Defaults to 0, no mode.
 * @param {any} [parent=null] - The parent of this Size component. Can be any object with public `width` and `height` properties. Dimensions are clamped to keep them within the parent bounds where possible.
 */
public class SizeTest {
    public float width, height;
    public float aspectRatio;

    private Size() {
        this(0f, 0f);
    }

    public Size(float w, float h) {
        width = w;
        height = h;
        aspectRatio = (height == 0f) ? 1f : width / height;
    }

    public static Size make(float w, float h) {
        return new Size(w, h);
    }

    public static Size zero() {
        return new Size(0f, 0f);
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public static boolean equalToSize(Size s1, Size s2) {
        return s1.width == s2.width && s1.height == s2.height;
    }

    public String toString() {
        return "<" + width + ", " + height + ">";
    }
}
