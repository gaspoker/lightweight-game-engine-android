package test.opengl;

import javax.microedition.khronos.opengles.GL10;
import android.opengl.GLSurfaceView;
import sdk.base.IGraphics;
import sdk.base.IPixmap;
import sdk.geometry.Vector;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static javax.microedition.khronos.opengles.GL10.GL_FLOAT;
import static javax.microedition.khronos.opengles.GL10.GL_LINES;
import static javax.microedition.khronos.opengles.GL10.GL_LINE_LOOP;
import static javax.microedition.khronos.opengles.GL10.GL_LINE_STRIP;
import static javax.microedition.khronos.opengles.GL10.GL_POINTS;
import static javax.microedition.khronos.opengles.GL10.GL_VERTEX_ARRAY;

public class GLGraphicsTest implements IGraphics {
    protected GLSurfaceView glView;
    protected GL10 gl;

    public GLGraphics(GLSurfaceView glView) {
        this.glView = glView;
    }

    public GL10 getGL() {
        return gl;
    }

    void setGL(GL10 gl) {
        this.gl = gl;
    }

    @Override
    public IPixmap newPixmap(String fileName, PixmapFormat format) {
        return null;
    }

    @Override
    public void clear(int color) {
        setColor(color);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        /*gl.glViewport(0, 0, getWidth(), getHeight());
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(0, GameScreen.VIEW_PORT_WIDTH, GameScreen.VIEW_PORT_HEIGHT, 0, 1, -1);*/
    }

    @Override
    public void drawPixel(float x, float y, int color, float alpha) {
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * 1);
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        vertices.put(x);
        vertices.put(y);

        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices);
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        gl.glDrawArrays(GL_POINTS, 0, 1);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    public void drawPoint(float x, float y, int color) {
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * 1);
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        vertices.put(x);
        vertices.put(y);

        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices); // size = 2
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color);
        gl.glPointSize(2);
        gl.glDrawArrays(GL_POINTS, 0, 1);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    public void drawPoint(Vector point, int color) {
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * 1);
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        vertices.put(point.x);
        vertices.put(point.y);

        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices); // size = 2
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color);
        gl.glPointSize(2);
        gl.glDrawArrays(GL_POINTS, 0, 1);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    public void drawPoints(Vector points[], int numberOfPoints) {
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * numberOfPoints);
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        for (int i = 0; i < numberOfPoints; i++) {
            vertices.put(points[i].x);
            vertices.put(points[i].y);
        }
        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices);
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        gl.glDrawArrays(GL_POINTS, 0, numberOfPoints);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }


    @Override
    public void drawLine(float x, float y, float x2, float y2, int color, float alpha) {
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * 2);
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        vertices.put(x);
        vertices.put(y);
        vertices.put(x2);
        vertices.put(y2);
        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices);
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        gl.glDrawArrays(GL_LINES, 0, 2);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    @Override
    public void drawRect(float x, float y, float width, float height, int color, float alpha) {
        List<Vector> poli = new ArrayList<>();

        poli.add(Vector.create(x, y));
        poli.add(Vector.create(x + width, y));
        poli.add(Vector.create(x + width, y + height));
        poli.add(Vector.create(x, y + height));

        setColor(color, alpha);
        drawPoly(poli, true, color, alpha);
    }

    public void drawPoly(List<? extends Vector> poli, boolean closePolygon, int color, float alpha) {
        int numberOfPoints = poli.size();
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * numberOfPoints);
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        for (int i = 0; i < numberOfPoints; i++) {
            vertices.put(poli.get(i).x);
            vertices.put(poli.get(i).y);
        }
        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices);
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        if (closePolygon)
            gl.glDrawArrays(GL_LINE_LOOP, 0, numberOfPoints);
        else
            gl.glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    public void drawCircle(float centerX, float centerY, float r, float a, int segments, boolean drawLineToCenter, int color, float alpha) {

        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * (segments + 2));
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        int additionalSegment = 1;

        if (drawLineToCenter)
            additionalSegment++;

        final float coef = 2.0f * (float) Math.PI / segments;

        for (int i = 0; i <= segments; i++) {
            float rads = i * coef;
            float j = (float) (r * Math.cos(rads + a) + centerX);
            float k = (float) (r * Math.sin(rads + a) + centerY);

            vertices.put(j);
            vertices.put(k);
        }
        vertices.put(centerX);
        vertices.put(centerY);

        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices);
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        setColor(color, alpha);
        gl.glDrawArrays(GL_LINE_STRIP, 0, segments + additionalSegment);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    public void drawQuadBezier(GL10 gl,float originX, float originY, float controlX, float controlY, float destinationX, float destinationY, int segments)
    {
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * (segments + 1));
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        float t = 0.0f;
        for(int i = 0; i < segments; i++)
        {
            float x = (float) Math.pow(1 - t, 2) * originX + 2.0f * (1 - t) * t * controlX + t * t * destinationX;
            float y = (float) Math.pow(1 - t, 2) * originY + 2.0f * (1 - t) * t * controlY + t * t * destinationY;
            vertices.put(x);
            vertices.put(y);
            t += 1.0f / segments;
        }
        vertices.put(destinationX);
        vertices.put(destinationY);

        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices);
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        gl.glDrawArrays(GL_LINE_STRIP, 0, segments + 1);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    public void drawCubicBezier(float originX, float originY, float control1X, float control1Y, float control2X, float control2Y, float destinationX, float destinationY, int segments)
    {
        ByteBuffer vbb = ByteBuffer.allocateDirect(4 * 2 * (segments + 1));
        vbb.order(ByteOrder.nativeOrder());
        FloatBuffer vertices = vbb.asFloatBuffer();

        float t = 0;
        for(int i = 0; i < segments; i++)
        {
            float x = (float) Math.pow(1 - t, 3) * originX + 3.0f * (float)Math.pow(1 - t, 2) * t * control1X + 3.0f * (1 - t) * t * t * control2X + t * t * t * destinationX;
            float y = (float) Math.pow(1 - t, 3) * originY + 3.0f * (float)Math.pow(1 - t, 2) * t * control1Y + 3.0f * (1 - t) * t * t * control2Y + t * t * t * destinationY;
            vertices.put(x);
            vertices.put(y);
            t += 1.0f / segments;
        }
        vertices.put(destinationX);
        vertices.put(destinationY);

        vertices.position(0);

        gl.glVertexPointer(2, GL_FLOAT, 0, vertices);
        gl.glEnableClientState(GL_VERTEX_ARRAY);

        gl.glDrawArrays(GL_LINE_STRIP, 0, segments + 1);

        gl.glDisableClientState(GL_VERTEX_ARRAY);
    }

    @Override
    public void drawPixmap(IPixmap pixmap, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) {

    }

    @Override
    public void drawPixmap(IPixmap pixmap, int x, int y) {

    }

    public int getWidth() {
        return glView.getWidth();
    }

    public int getHeight() {
        return glView.getHeight();
    }

    public void setColor(int color) {
        GLColor glColor = new GLColor(color);
        gl.glColor4f(glColor.r, glColor.g, glColor.b, glColor.a);
    }

    public void setColor(int color, float alpha) {
        GLColor glColor = new GLColor(color);
        gl.glColor4f(glColor.r, glColor.g, glColor.b, alpha);
    }

    public void enableTexture2D() {
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL10.GL_TEXTURE_2D);
    }

    public void disableTexture2D() {
        gl.glDisable(GL10.GL_TEXTURE_2D);
        gl.glDisable(GL10.GL_BLEND);
    }

    /*public void translate(int x, int y) {
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslatef(x, y, 0);
    }*/


}
