package test.opengl;

public class GLAnimationTest {
    private final GLTextureRegion[] keyFrames;
    private final float frameDuration;
    private boolean repeat;
    private boolean hideOnComplete;
    private boolean onComplete;

    public final float width, height;

    public interface AnimationListener {
        void onComplete();
    }
    protected AnimationListener listener;

    public GLAnimation(float frameDuration, boolean repeat, boolean hideOnComplete, GLTextureRegion... keyFrames) {
        this.frameDuration = frameDuration;
        this.repeat = repeat;
        this.hideOnComplete = hideOnComplete;
        this.keyFrames = keyFrames;

        // Events
        this.onComplete = false;
        this.width = keyFrames[0].width;
        this.height = keyFrames[0].height;
    }
    
    //public GLTextureRegion getKeyFrame(float stateTime, int mode) {
    public GLTextureRegion getKeyFrame(float stateTime) {
        int frameNumber = (int)(stateTime / frameDuration);

        if (frameNumber > keyFrames.length-1 && hideOnComplete) {
            if (listener != null && !onComplete) {
                onComplete = true;
                listener.onComplete();
            }
            return null;
        }

        if (frameNumber <= keyFrames.length-1)
            onComplete = false;

        if(!repeat) {
            frameNumber = Math.min(keyFrames.length-1, frameNumber);            
        } else {
            frameNumber = frameNumber % keyFrames.length;
        }

        return keyFrames[frameNumber];
    }

    public void setAnimationListener(AnimationListener listener) {
        this.listener = listener;
    }

    public int getFramesLength() {
        return keyFrames.length;
    }

}
