package test.opengl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import sdk.base.IFileIO;
import sdk.base.IGame;
import sdk.base.IGraphics;
import sdk.base.IAudio;
import sdk.base.IInput;
import sdk.collision.Collider;
import sdk.core.Scene;
import sdk.base.Audio;
import sdk.base.FileIO;
import sdk.base.Input;
import sdk.geometry.Bounds;

public abstract class GLGameTest extends Activity implements IGame, Renderer {
    public enum GLGameState {
        Initialized,
        Running,
        Paused,
        Finished,
        Idle
    }

    GLSurfaceView glView;    
    GLGraphics graphics;
    IAudio audio;
    public IInput input;
    IFileIO fileIO;
    Scene scene;
    public GLGameState state = GLGameState.Initialized;
    Object stateChanged = new Object();
    long startTime = System.nanoTime();
    public float width;
    public float height;
    public float scaleX;
    public float scaleY;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                             WindowManager.LayoutParams.FLAG_FULLSCREEN);
        glView = new GLSurfaceView(this);
        glView.setRenderer(this);
        setContentView(glView);
        graphics = new GLGraphics(glView);
        fileIO = new FileIO(this);
        audio = new Audio(this);
        input = new Input(this, glView, 1, 1);

        //loadAssets();
    }

    @Override
    public void onResume() {
        super.onResume();
        glView.onResume();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        graphics.setGL(gl);
        loadAssets();

        synchronized(stateChanged) {
            if(state == GLGameState.Initialized) {
                width = graphics.getWidth();
                height = graphics.getHeight();

                scene = getMainScene();
            }
            state = GLGameState.Running;
            scene.resume();
            startTime = System.nanoTime();
        }
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
    }

    public void onDrawFrame(GL10 gl) {
        GLGameState state = null;

        synchronized(stateChanged) {
            state = this.state;
        }

        if(state == GLGameState.Running) {
            float deltaTime = (System.nanoTime() - startTime) / 1000000000.0f; // in seconds
            startTime = System.nanoTime();
            scene.update(deltaTime);
            scene.collider.update(startTime, deltaTime);
            scene.present(deltaTime);
            scene.collider.present(deltaTime);
        }

        if(state == GLGameState.Paused) {
            scene.pause();
            synchronized(stateChanged) {
                this.state = GLGameState.Idle;
                stateChanged.notifyAll();
            }
        }

        if(state == GLGameState.Finished) {
            scene.pause();
            scene.dispose();
            synchronized(stateChanged) {
                this.state = GLGameState.Idle;
                stateChanged.notifyAll();
            }
        }
    }

    @Override
    public void onPause() {
        synchronized(stateChanged) {
            if(isFinishing())
                state = GLGameState.Finished;
            else
                state = GLGameState.Paused;
            while(true) {
                try {
                    stateChanged.wait();
                    break;
                } catch(InterruptedException e) {
                }
            }
        }
        glView.onPause();
        super.onPause();
    }

    public GLGraphics getGLGraphics() {
        return graphics;
    }

    public GLSurfaceView getGLView() {
        return glView;
    }

    public IInput getInput() {
        return input;
    }

    public IFileIO getFileIO() {
        return fileIO;
    }

    public IGraphics getGraphics() {
        throw new IllegalStateException("We are using OpenGL!");
    }

    public IAudio getAudio() {
        return audio;
    }

    public void setScene(Scene newScene) {
        if (newScene == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.scene.pause();
        this.scene.dispose();
        newScene.resume();
        newScene.update(0);
        this.scene = newScene;
    }

    public Scene getScene() {
        return scene;
    }

    // Full Screen Immersive Mode
    // https://developer.android.com/training/system-ui/immersive.html#java
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }



}
