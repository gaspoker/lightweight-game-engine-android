package test.opengl;

import sdk.base.IGame;
import sdk.core.Scene;

public abstract class GLSceneTest extends Scene {
    protected final GLGraphics glGraphics;
    protected final GLGame glGame;
    
    public GLSceneTest(IGame game) {
        super(game);
        glGame = (GLGame)game;
        glGraphics = ((GLGame)game).getGLGraphics();
    }
}
