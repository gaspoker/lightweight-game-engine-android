package test.base;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class AudioTest implements IAudio {
    AssetManager assets;
    SoundPool soundPool;
    List<MediaPlayer> musicPool;

    public Audio(Activity activity) {
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(30, AudioManager.STREAM_MUSIC, 0); // maxStreams = 20
        this.musicPool = new ArrayList<>();
    }

    public IMusic newMusic(String filename) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            MediaPlayer mediaPlayer = new MediaPlayer();
            musicPool.add(mediaPlayer);
            return new Music(mediaPlayer, assetDescriptor);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load music '" + filename + "'");
        }
    }

    public ISound newSound(String filename) {
    //public ISound newSound(String filename, long duration) {
        try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            int soundId = soundPool.load(assetDescriptor, 0);
            //return new Sound(soundPool, soundId, duration);
            return new Sound(soundPool, soundId);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load sound '" + filename + "'");
        }
    }

    public void pause() {
        this.soundPool.autoPause();
        for (MediaPlayer music : musicPool) {
            music.setVolume(0, 0);
        }
    }

    public void stopAll() {
        this.soundPool.autoPause(); // AutoPause reemplazaría al Stop para el SoundPool sin problemas.
        for (MediaPlayer music : musicPool) {
            if (music.isPlaying()) music.stop();
        }
    }

    public void resume() {
        this.soundPool.autoResume();
        /*for (MediaPlayer mp : musicPool) {
            mp.start();
        }*/
    }

    public boolean isPlayingMusic() {
        for (MediaPlayer music : musicPool) {
            if (music.isPlaying()) return true;
        }
        return false;
    }

}