package test.base;

import android.media.SoundPool;

import java.util.Timer;
import java.util.TimerTask;

public class SoundTest implements ISound {
    int soundId;
    SoundPool soundPool;
    int streamID;

    public Sound(SoundPool soundPool, int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool;
        this.streamID = 0;
    }

    public void play(float volume, boolean isLooping) {
        if (this.streamID == 0) {
            this.streamID = soundPool.play(soundId, volume, volume, 0, (isLooping) ? -1 : 0, 1);
        }
    }

    public void pause() {
        if (this.streamID != 0) {
            soundPool.pause(streamID);
        }
    }

    /*public void resume() {
        if (this.streamID != 0) {
            soundPool.resume(streamID);
        }
    }*/

    public void stop() {
        if (this.streamID != 0) {
            soundPool.stop(streamID);
            this.streamID = 0;
        }
    }

    /*public void pauseAll() {
        soundPool.autoPause();
    }*/

    public void dispose() {
        soundPool.unload(soundId);
    }
}
