![Lightweight Game Engine Logo](banner.png)

# Lightweight Game Engine - Mobile Java & OpenGL ES version for Android

Lightweight Game Engine (LGE) is an open-source engine for 2D game development, especially tailored for pixel art games. It is inspired by techniques used in classic consoles (scenes, tile backgrounds, sprite-sheets, texture regions, collisions, physics, etc.).

## LGE SDK 2.0

MIT Licensed Open Source version of LGE from Gaspo Soft. Maintained by the Gaspo Soft team and contributions from the community.

Dedicated to 2D game development, LGE is an extremely powerful, flexible, and fast Java engine which has been used and tested in many games. This version is designed for Android devices. It includes an OpenGL ES 1.1 batched rendering system, a full Physics system based on Matter.js, OpenAL audio, skeletal and spritesheet animation, automated asset management and a modular project structure.

### What's New?

LGE SDK 2.0 is currently in progress. The major change with 2.0 it is the replacement of the basic collision system of version 1.1 for the incorporation of a full physics system based on the 2D physics engine Matter.js.

### LGE Features

  __Input Methods:__
   - Device keyboard
   - Accelerometer
   - X-input standard Gamepads (in progress)
   
  __Graphics:__ 
   - Textures of various sizes
   - Large backgrounds using tiles
   - Sprites with or without animation
   - 2D virtual camera
   - Text layers
   - Canvas for primitive drawing
   - Ability to move, rotate, and scale all graphic elements
   - Support for 3D graphics (in progress)
   
  __Sound:__
   - Up to 4 simultaneous music/dialogue streams (ogg and mp3 format)
   - Up to 64 simultaneous sound effects
   - 5-channel mixer
   - Ability to change volume, pan (for sound effects), pitch, and loop
   
  __Physics:__
   - Rigid, compound and composite bodies
   - Concave and convex hulls
   - Physical properties (mass, area, density etc.)
   - Restitution (elastic and inelastic collisions)
   - Conservation of momentum, friction and resistance
   - Constraints, gravity, sleeping and static bodies
   - Rounded corners (chamfering)
   
  __Collisions:__
   - Collision detection between 2 sprites by boxes and irregular shapes, with the ability to add multiple colliders to each sprite
   - Pixel-level collision detection between 2 sprites
   - Raycasting from 1 point of a coordinate to a sprite
   - Broad-phase, mid-phase and narrow-phase
   - Collision queries (raycasting, region tests)

  __File System:__
   - Loading functions for resources from the file system
   - Ability to create resource packaging files with encryption options
   - Loading and saving functions for binary or text files
   - Ability to manage up to 127 resource repositories, specifying the list of files to load from a plain text file

### Installation
#### Clone repo (and specify folder name):
```
git clone https://gitlab.com/gaspoker/lightweight-game-engine-android.git PROJECT_NAME
```

#### Navigate in folder:
```
cd PROJECT_NAME
```

#### A suggested folder structure for your new project could be:
```
PROJECT_NAME/
├─ sdk/
│  ├─ base/
│  ├─ canvas/
│  ├─ collision/
│  ├─ core/
│  ├─ geometry/
│  ├─ opengl/
│  ├─ utils/
├─ main/             
│  ├─ assets/    <- your game assets here (sprites, tile mpas, audios, fonts, etc.)
│  ├─ models/    <- the game objects classes here
│  ├─ scenes/    <- the game scenes classes here
│  ├─ ...
├─ test/         <- your unit tests here
│  ├─ ...
├─ README.md
```

### Frequent Questions

- __Which programming language is used?__
  The entire engine is programmed in Java, using OpenGL ES 1.1 library.

- __Who is it intended for?__
  It is aimed at anyone who wants to develop their own 2D video game and has a basic understanding of object-oriented programming.

- __Which systems does it run on?__
  Currently, the engine runs on Android. Gaspo Soft team is working on a multiplatform porting developed in C++.

- __Under what license is it distributed?__
  The engine is distributed under the MIT license. This means you can use, modify, create, and distribute content using the engine, as long as proper attribution is given.

- __How did this project originate?__
  The project was born out of the need for a custom 2D tool for platforms game projects and, to a greater extent, as a foundation for my video game development series.

- __In what development state is the engine?__
  With over 7 years of development, it is now in a stable phase without known major bugs. The current focus is on performance improvement and the addition of new features.

- __What does the engine distribution package contain?__
  The file includes the engine's source code, as well as compiled libraries for Android devices.

- __How can I contact the author?__
  You can send an email to the contact address: gaspoker@gmail.com.

Here's a demo with many of the engine's features:
[Parsec Reloaded](https://play.google.com/store/apps/details?id=com.gasposoft.parsec)

![Gaspo Soft Logo](gaspo-soft.png)